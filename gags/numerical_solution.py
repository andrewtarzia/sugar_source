#!/usr/bin/env python

"""
Calculate the numerical solution to the PB equation.

Author: Andrew Tarzia

"""

import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt

import parameters


def main():
    solutions = {
        'MAF7': {'pH': 5.5, 'linestyle': '-'},
        'ZIF90': {'pH': 7, 'linestyle': '-.'},
        'ZIF8': {'pH': 10, 'linestyle': '--'},
    }
    # Get debye length of solution.
    for solution in solutions:
        print("-------------------------------")
        print(f'solution: {solution}')
        fig1, ax1 = plt.subplots(figsize=(8, 6))
        fig2, ax2 = plt.subplots(figsize=(8, 6))
        fig3, ax3 = plt.subplots(figsize=(8, 6))
        sol = parameters.Solution(
            pH=solutions[solution]['pH'],
            salt1_conc=0.08,
            salt2_conc=0.16,
            salt1_Z=2,
            salt2_Z=-1
        )

        # Iterate over polymers.
        polymers = ['HA', 'CS', 'DS', 'HP']
        for polymer in polymers:
            print("-------------------------------")
            poly = parameters.Polymer(
                type=polymer,
                no_disacc=50,
                conc=0.36
            )
            print(f"polymer: {poly.properties['figure_name']}")
            model = parameters.CellModel(
                polymer=poly,
                solution=sol,
                R_end=25,
            )
            model.phi = model.initial_guess(x=model.x_space)
            # Solve PDE.
            model.pde_solution = integrate.solve_bvp(
                fun=model.reduced_PB_expression,
                bc=model.boundary_conditions,
                x=model.x_space,
                y=model.phi,
                max_nodes=10000,
                verbose=False
            )
            print(
                model.pde_solution.success,
                model.pde_solution.message,
                model.pde_solution.status
            )

            # Plot the potential as a function of r.
            # print(model.solution.y)
            # print(model.interpolate_solution())
            phi, dphi, ddphi = model.interpolate_solution()
            ax1.plot(
                model.x_space,
                phi,
                c=poly.colour,
                linestyle=solutions[solution]['linestyle'],
                lw=2,
                label=(
                    # f"{solution} (pH = {solutions[solution]['pH']}):"
                    f" {polymer}"
                )
                # label=f"{solution}: {poly.properties['figure_name']}"
            )

            # Calculate psi.
            psi = model.unreduce_phi(phi)
            ax2.plot(
                model.r_space,
                psi,
                c=poly.colour,
                linestyle=solutions[solution]['linestyle'],
                lw=2,
                label=(
                    # f"{solution} (pH = {solutions[solution]['pH']}):"
                    f" {polymer}"
                )
                # label=f"{solution}: {poly.properties['figure_name']}"
            )
            # Calculate ion concentration at r = a.
            ion_concs = model.calc_ion_concentrations(potential=psi)
            ax3.plot(
                model.r_space,
                ion_concs[0]/model.solution.salt1_conc,
                c=poly.colour,
                linestyle=solutions[solution]['linestyle'],
                lw=2,
                label=(
                    # f"{solution} (pH = {solutions[solution]['pH']}):"
                    f" {polymer}"
                )
                # label=f"{solution}: {poly.properties['figure_name']}"
            )

            # Calculate the average zinc ion concentration within one
            # zinc radius (0.74 angstrom) of the carbohydrate.
            avg_conc = np.average(
                ion_concs[0][model.r_space <= model.a+0.74]
            )
            print(f'average Zn2+ concention = {avg_conc} mol/L')
            print(
                'average Zn2+ enhancement = '
                f'{avg_conc/model.solution.salt1_conc}'
            )

        ax1.tick_params(axis='both', which='major', labelsize=20)
        ax1.set_xlabel("$x$", fontsize=18)
        ax1.set_ylabel(r"$\phi$", fontsize=18)
        ax1.axhline(y=0.0, c='k', lw=2)
        ax1.set_xlim(0, max(model.x_space))
        # ax.set_ylim(-65, 65)
        ax1.axvline(x=model.a_r, c='k', lw=2, linestyle='--')
        ax1.axvspan(
            xmin=0,
            xmax=model.a_r,
            facecolor='k',
            alpha=0.2
        )
        ax1.axvspan(
            xmin=model.a_r,
            xmax=model.a_r+0.74*model.kappa,
            facecolor='#DB869D',
            alpha=0.1
        )

        ax1.legend(fancybox=True, ncol=1, fontsize=16)
        fig1.tight_layout()
        fig1.savefig(
            f'phi_vs_x_{solution}.pdf',
            dpi=720,
            bbox_inches='tight'
        )

        ax2.tick_params(axis='both', which='major', labelsize=20)
        ax2.set_xlabel(r"$r$ [$\mathrm{\AA}$]", fontsize=18)
        ax2.set_ylabel(r"$\psi$ [mV]", fontsize=18)
        ax2.axhline(y=0.0, c='k', lw=2)
        ax2.set_xlim(0, max(model.r_space))
        # ax.set_ylim(-65, 65)
        ax2.axvline(x=model.a, c='k', lw=2, linestyle='--')
        ax2.axvspan(
            xmin=0,
            xmax=model.a,
            facecolor='k',
            alpha=0.2
        )
        ax2.axvspan(
            xmin=model.a,
            xmax=model.a+0.74,
            facecolor='#DB869D',
            alpha=0.1
        )

        ax2.legend(fancybox=True, ncol=1, fontsize=16)
        fig2.tight_layout()
        fig2.savefig(
            f'psi_vs_r_{solution}.pdf',
            dpi=720,
            bbox_inches='tight'
        )

        ax3.tick_params(axis='both', which='major', labelsize=20)
        ax3.set_xlabel(r"$r$ [$\mathrm{\AA}$]", fontsize=18)
        ax3.set_ylabel("zinc ion enhancement", fontsize=18)
        ax3.axhline(y=1.0, c='k', lw=2)
        ax3.set_xlim(0, max(model.r_space))
        # ax.set_ylim(-65, 65)
        ax3.axvline(x=model.a, c='k', lw=2, linestyle='--')
        ax3.axvspan(
            xmin=0,
            xmax=model.a,
            facecolor='k',
            alpha=0.2
        )
        ax3.axvspan(
            xmin=model.a,
            xmax=model.a+0.74,
            facecolor='#DB869D',
            alpha=0.1
        )

        ax3.legend(fancybox=True, ncol=1, fontsize=16)
        fig3.tight_layout()
        fig3.savefig(
            f'enh_vs_r_{solution}.pdf',
            dpi=720,
            bbox_inches='tight'
        )


if __name__ == '__main__':
    main()
