#!/usr/bin/env python

"""
Setup script containing variables for numerical solution of PB equation
for an ion-permeable sphere.

Author: Andrew Tarzia

"""

import numpy as np
from scipy import constants
import expressions_sphere_script as exp_s

# constants
KB = constants.Boltzmann  # boltzmann constant [J/K]
NA = constants.N_A  # avogadros number [mol-1]
e = constants.elementary_charge  # elementary charge [C]
E_0 = constants.epsilon_0  # Vacuum permittivity [F/m = C^2/(J.m)]
M_TO_NM = 1.0e9  # conversion from m to nm
M3_TO_L = 1.0e3  # conversion from m^3 to L
PI = constants.pi


def polymer_prop(verbose=True):
    """Define properties of polymer.

    """
    # polymer properties
    # Lc is approximate monomer length (length of Glucose unit)
    # Lc = 5  # angstrom
    # using value from "Polysaccharide elasticity governed by
    # chair±boat transitions of the glucopyranose ring"
    Lc = 4.4  # angstrom
    # DP is the number of monomers in carbohydrate chain
    # or degree of polymerisation DP
    # set such that MW is around 10 kDa
    DP = 60
    # from literature:
    # - Carboxymethyl and native dextran behave as freely jointed
    # chains
    # - Kuhn length is approximately the same as the length of the
    # glucose mono
    # calculate radius of gyration Rg using equation for Freely
    # Jointed Chain
    Rg_2 = (DP * (Lc ** 2)) / 6
    Rg = np.sqrt(Rg_2)
    # calculate the pervaded volume as the volume of a sphere with
    # radius == Rg
    Vg = (4/3) * PI * (Rg ** 3)
    if verbose:
        print("-------------------------------")
        print("radius of gyration =", round(Rg, 3), "angstrom")
        print("pervaded volume =", round(Vg, 3), "angstrom^3")
        print("-------------------------------")

    # calculate and output packing properties of polymers
    # mass of anhydrous glucose monomer
    m_mon = 162  # g/mol
    # concentration of dextran
    CD = 0.72  # g/L
    MW = m_mon * DP  # g/mol
    # number concentration of dextran
    num_CD = CD / (MW)  # mol/L
    num_CD = num_CD * M3_TO_L  # mol/m3
    num_CD = num_CD * NA  # m^-3
    num_CD = num_CD * (10 ** -30)  # angstrom^-3
    # get average separation of polymer chains
    avg_sep = (num_CD) ** (-1/3)
    if verbose:
        print("-------------------------------")
        print("dextran MW =", MW, 'g/mol')
        print('number concentration =',
              '%e' % num_CD, 'molecules / angstrom^3')
        print('avg. polymer separation =',
              '%e' % avg_sep, 'angstrom')
        print("-------------------------------")

    # set functionalisation density as degree of substitution:
    # DS = avg number of substituted OH for R per glucose
    # (max at 3, for 3 OH per glucose)
    # vary the degree of substitution
    # DS = [0, 0.02, 0.1]
    # DS = [0, 0.02, 0.05, 0.1, 0.5, 1.0, 2.0, 3.0]
    DS = [0.01, 0.02, 0.04, 0.06, 0.08, 0.1,
          0.125, 0.15, 0.175, 0.2,
          0.225, 0.25, 0.275, 0.3,
          0.325, 0.35, 0.375, 0.4,
          0.425, 0.45, 0.475, 0.5,
          0.525, 0.55, 0.575, 0.6,
          0.625, 0.65, 0.675, 0.7,
          0.725, 0.75, 0.775, 0.8,
          0.825, 0.85, 0.875, 0.9,
          0.925, 0.95, 0.975, 1.0]
    # DS = np.append(np.linspace(0, 0.05, 2), np.linspace(0.1, 1.0, 5))

    return Rg, Vg, DS, DP, Lc


def solution_properties(verbose=True):
    """Define solution properties.

    """
    # solution properties
    # set temp
    T = 298  # [K]
    # set relative permittivity [unit less] -- water
    E_r = 80
    # set pH of solution
    PH = 11
    # salt concs (1 = Zn2+, 2 = acetate1-)
    salt1_conc = 0.04  # mol/L
    salt2_conc = 0.08  # mol/L
    # ion valencies
    Z1 = 2  # zinc 2+
    Z2 = -1  # co-ion

    # calculate ionic strength
    # note that this does not include the OH-, H+ (based on pH).
    # based on DOI: 10.1039/C5RA04033G:
    # there is a hydrolysis equilibrium between H2O and HMIM
    # that produces OH- and H2MIM+ and causes the high pH.
    # at pH 11, [OH-] (and its counter ion) == 1E-3 M
    # which would have a very small impact, nonetheless, I mistakenly
    # did not include it here, and I am keeping this code as is to
    # create the data seen in the Materials Horizons publication.
    # I have rerun all calculations with all ions considered and found
    # no impact on the final result.
    # units in mol/L
    I_mol = 0.5 * ((salt1_conc * Z1 * Z1) + (salt2_conc * Z2 * Z2))

    # in molec per m3
    I_m3 = NA * I_mol * M3_TO_L  # molecules / m3

    # calculate kappasq in units m^-2
    kappasq = exp_s.calculate_kappasq(e, I_m3, E_0, E_r, KB, T, PI)
    kappa = np.sqrt(kappasq)  # m^-1
    kappa_ang = kappa * (10 ** -10)

    if verbose:
        print("-------------------------------")
        print('I =', I_mol, 'mol/L')
        print('I =', I_m3, 'm^-3')
        print("-------------------------------")
        print('kappa =', kappa_ang, 'angstrom^-1')
        print('debye length =',
              round(1/(kappa_ang), 3), 'angstrom')
        print("-------------------------------")

    sol_prop = {}
    sol_prop['kappa_a'] = kappa_ang
    sol_prop['T'] = T
    sol_prop['PH'] = PH
    sol_prop['E_r'] = E_r
    sol_prop['c1'] = salt1_conc
    sol_prop['z1'] = Z1
    sol_prop['c2'] = salt2_conc
    sol_prop['z2'] = Z2
    sol_prop['I_m3'] = I_m3
    return sol_prop


def func_prop():
    """Define the functionalisations to be used.

    """
    # functionalisation properties
    # set plot colours for the different functionalities
    func_colors = {"carboxymethyl": 'royalblue',
                   "amine": 'firebrick'}
    # set charge and pka of functionalisations
    func_charges = {"carboxymethyl": -1, "amine": +1}
    func_pkas = {"carboxymethyl": 4, "amine": 10.64}
    # NH2 pKa: DOI:10.1021/je500680q for triethylamine at 298 K
    # COO pka: No specific paper, but many suggest a value of 4 is
    # sufficient

    # set width of smooth function to describe volume charge density
    # w defines the width of the tanh function
    w = 0.02
    return func_colors, func_charges, func_pkas, w


def charge_properties(Vg, ds, func, func_charges, func_pkas, PH, DP):
    """Get charge properties of polymer for a given volume and ds.

    """
    # the total number of charge centres on the chain based on the DS
    # and DP
    func_count = ds * DP
    # calculate cylinder charge of each functionalisation based on pH
    pka = func_pkas[func]
    charge = func_charges[func]
    # print(charge)
    partial_charge = exp_s.calculate_func_charge(charge, PH, pka)
    # print(partial_charge)
    func_total_charges = func_count * partial_charge
    # print(func_total_charges)
    return func_total_charges
