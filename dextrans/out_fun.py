#!/usr/bin/env python

"""
Functions for plotting and output.

Author: Andrew Tarzia

"""

import matplotlib.pyplot as plt
import pandas as pd
from expressions_sphere_script import calc_three_integrands
import numpy as np


def plot_charge_properties(filename1, filename2, func_charges,
                           func_colors, func_total_charges, Vg, DS):
    """Plot the volume charge density + total charge as a fucntion of DS

    Arguments:
        filename (str) : output PDF file
        func_charges (dict) : dict of func charge sign
        func_colours (dict) : dict of plot colors
        func_total_charges (dict) : dict of total charge values per DS
        Vg (float) : Pervaded volume in angstrom^-3
        DS (numpy array) : DS valules

    """

    fig, ax1 = plt.subplots(figsize=(8, 6))
    ax1.tick_params(axis='both', which='major', labelsize=20)
    for func in func_charges.keys():
        ax1.plot(DS, func_total_charges[func],
                 c=func_colors[func], label=func, lw=2)

    # ax1.set_title('volume charge density', fontsize=18)
    ax1.set_xlabel("degree of substitution", fontsize=18)
    ax1.set_ylabel("carbohydrate charge [e]", fontsize=18)
    ax1.axvline(x=0.125, c='k', lw=2)
    ax1.set_xlim(0, 1)
    ax1.set_ylim(-65, 65)

    ax1.legend(fancybox=True, ncol=1,
               fontsize=16)
    fig.tight_layout()
    fig.savefig(filename1,
                dpi=720, bbox_inches='tight')

    fig, ax1 = plt.subplots()
    ax1.tick_params(axis='both', which='major', labelsize=20)
    for func in func_charges.keys():
        # calculate volume charge density as total charge / Volume
        Vg_q = func_total_charges[func] / Vg  # e/angstrom^3
        ax1.plot(DS, Vg_q, c=func_colors[func], label=func, lw=2)

    # ax1.set_title('volume charge density', fontsize=18)
    ax1.set_xlabel("degree of substitution", fontsize=18)
    ax1.set_ylabel(r'$\rho_s$ [e/$\mathrm{\AA}^3$]', fontsize=18)
    ax1.axvline(x=0.125, c='k', lw=2)
    ax1.set_xlim(0, 1)
    ax1.set_ylim(-0.006, 0.006)

    ax1.legend(fancybox=True, ncol=1,
               fontsize=16)
    fig.tight_layout()
    fig.savefig(filename2,
                dpi=720, bbox_inches='tight')


def initialise_final_plot(func_colors=None, plot_analytic=None,
                          surf_pot=None):
    """ Initialise and return object for final plot.

    """
    if surf_pot is None:
        fig = plt.figure()
        ax1 = plt.subplot()
        ax1.tick_params(axis='both', which='major', labelsize=20)
        # decoy legend
        if func_colors is not None:
            dec = np.linspace(-1000, -999, 2)
            for func in func_colors:
                if plot_analytic is True:
                    ax1.plot(dec, dec, c=func_colors[func],
                             linestyle='--',
                             label=func+' - analytical')
                ax1.plot(dec, dec, c=func_colors[func],
                         label=func+' - numerical')
            ax1.legend()

        return fig, ax1, 0, 0
    else:
        fig = plt.figure(figsize=(16, 5))
        ax1 = plt.subplot(131)
        ax1.tick_params(axis='both', which='major', labelsize=20)
        # label
        ax1.text(0.01, 0.9, '(a)',
                 verticalalignment='bottom',
                 horizontalalignment='left',
                 transform=ax1.transAxes,
                 fontsize=18)
        #
        ax2 = plt.subplot(132)
        ax2.tick_params(axis='both', which='major', labelsize=20)
        # # label
        ax2.text(0.01, 0.9, '(b)',
                 verticalalignment='bottom',
                 horizontalalignment='left',
                 transform=ax2.transAxes,
                 fontsize=18)
        #
        ax3 = plt.subplot(133)
        ax3.tick_params(axis='both', which='major', labelsize=20)
        # label
        ax3.text(0.01, 0.9, '(c)',
                 verticalalignment='bottom',
                 horizontalalignment='left',
                 transform=ax3.transAxes,
                 fontsize=18)

        # decoy legend
        if func_colors is not None:
            dec = np.linspace(-1000, -999, 2)
            for func in func_colors:
                if plot_analytic is True:
                    ax1.plot(dec, dec, c=func_colors[func],
                             linestyle='--',
                             label=func+' - analytical')
                ax1.plot(dec, dec, c=func_colors[func],
                         label=func+' - numerical')
            ax1.legend()

        return fig, ax1, ax2, ax3


def finalise_final_plot(fig, ax1, ax2, ax3, filename, surf_pot=None):
    """ Finalise plot and output to filename.

    """
    if surf_pot is None:
        ax1.set_ylabel(r"$\psi(r)$ [mV]", fontsize=18)
        ax1.set_xlabel(r"$r$ [$\mathrm{\AA}$]", fontsize=18)
        # ax1.set_ylabel("$\phi(x)$ ", fontsize=18)
        # ax1.set_xlabel("$x$", fontsize=18)

    else:
        ax1.set_ylabel(r"$\psi(r)$ [mV]", fontsize=18)
        ax1.set_xlabel(r"$r$ [$\mathrm{\AA}$]", fontsize=18)
        # ax1.set_ylabel("$\phi(x)$ ", fontsize=18)
        # ax1.set_xlabel("$x$", fontsize=18)
        ax2.set_ylabel(r"$<\psi(R_\mathrm{g})>$ [mV]", fontsize=18)
        ax2.set_xlabel(r"DS", fontsize=18)
        ax2.axvline(x=0.125, c='k', lw=2)
        # ax3.set_ylabel("log Zn$^{2+}$ enhancement", fontsize=18)
        ax3.set_ylabel(r"Zn$^{2+}$ enhancement", fontsize=18)
        ax3.set_xlabel(r"DS", fontsize=18)
        ax3.axvline(x=0.125, c='k', lw=2)
        # ax3.set_yscale("symlog", nonposy='clip')

    fig.tight_layout()
    fig.savefig(filename, dpi=720, bbox_inches='tight')


def initialise_wall_plot():
    """ Initialise and return object for final plot.

    """

    fig = plt.figure(figsize=(5, 8))
    ax1 = plt.subplot()
    ax1.tick_params(axis='both', which='major', labelsize=20)

    return fig, ax1


def finalise_wall_plot(fig, ax1, filename):
    """ Finalise plot and output to filename.

    """

    # ax2.set_title(r'surface potential', fontsize=18)
    # ax2.set_xlabel("distance from cylinder surface [$\mathrm{\AA}$]",
    # fontsize=18)
    ax1.set_ylabel(r"$\psi$", fontsize=18)
    ax1.set_xlabel(r"$x$", fontsize=18)

    fig.tight_layout()
    fig.savefig(filename, dpi=720, bbox_inches='tight')


def plot_phi_cf(x_out, solution, x0, AX0, z_1, n_1, z_2, n_2, I_m3):
    """Plot comparison of phi, LHS and RHS.

    """
    import matplotlib.pyplot as plt
    fig_psi, ax_psi = plt.subplots()
    # reduced electrostatic potential
    # use cubic spline of solution to get
    # reduced potential as function x
    psir = solution.sol(x_out)[0]
    # first derivative of spline
    dpsir = solution.sol.derivative(nu=1)(x_out)[0]

    # second derivative of spline
    d2psir = solution.sol.derivative(nu=2)(x_out)[0]

    # plot the RHS (solution) and its derivatives
    # ax_psi.plot(x_out, psir, c='k', label='$\phi$')
    # ax_psi.plot(x_out, dpsir, c='r', label="$\phi$'")
    # ax_psi.plot(x_out, d2psir, c='b', label="$\phi$''")

    # plot the LHS
    LHS = d2psir + (2 / x_out) * dpsir
    ax_psi.plot(x_out, LHS, c='k', label="LHS",
                linestyle='--')

    # plot the RHS
    ax = np.zeros(x_out.size)
    ax[x_out <= x0] = AX0
    inner = (z_1 * n_1 * np.exp(-z_1 * psir) + z_2 * n_2 * np.exp(-z_2 * psir))
    RHS = (-inner / (2 * I_m3)) - (1 / 2) * ax
    ax_psi.plot(x_out, RHS, c='purple', label="RHS",
                linestyle='-.')

    ax_psi.legend()
    ax_psi.tick_params(axis='both', which='major', labelsize=20)
    # ax_psi.set_ylabel("$\phi(x)$", fontsize=18)
    ax_psi.set_ylabel("LHS or RHS", fontsize=18)
    ax_psi.set_xlabel("$x$", fontsize=18)

    fig_psi.tight_layout()
    fig_psi.savefig('psi_cf.pdf', dpi=720, bbox_inches='tight')
    plt.close()


def plot_HF_diff(x_out, solution, x0, AX0, z_1,
                 n_1, z_2, n_2, I_m3, ds):
    """Plot difference of LHS and RHS after plugging in solution.

    """
    import matplotlib.pyplot as plt
    fig_psi, ax_psi = plt.subplots()
    # reduced electrostatic potential
    # use cubic spline of solution to get
    # reduced potential as function x
    psir = solution.sol(x_out)[0]
    # first derivative of spline
    dpsir = solution.sol.derivative(nu=1)(x_out)[0]

    # second derivative of spline
    d2psir = solution.sol.derivative(nu=2)(x_out)[0]

    # calculate LHS
    LHS = d2psir + (2 / x_out) * dpsir

    # calculate RHS
    ax = np.zeros(x_out.size)
    ax[x_out <= x0] = AX0
    inner = (z_1 * n_1 * np.exp(-z_1 * psir) + z_2 * n_2 * np.exp(-z_2 * psir))
    RHS = (-inner / (2 * I_m3)) - (1 / 2) * ax

    # plot difference
    ax_psi.plot(x_out, LHS-RHS, c='k', label="LHS-RHS",
                linestyle='-')

    ax_psi.legend()
    ax_psi.tick_params(axis='both', which='major', labelsize=20)
    ax_psi.set_ylabel("LHS - RHS", fontsize=18)
    ax_psi.set_xlabel("$x$", fontsize=18)
    ax_psi.set_title('DS = '+str(ds))
    ax_psi.set_ylim(-20, 20)

    fig_psi.tight_layout()
    fig_psi.savefig('HS_diff_'+str(ds*10)+'.pdf',
                    dpi=720, bbox_inches='tight')
    plt.close()


def visualize_AXF(AX0, x_in, x0, w, ds):
    """Plot A(x) function for testing and output integral.

    """
    import matplotlib.pyplot as plt

    AX_f, AX_i, AX_i_analytic, AX_a = calc_three_integrands(AX0, x_in,
                                                            x0, w, ds)
    print('integral of A(x) =', AX_i)
    print('analytical integral of A(x) =', AX_i_analytic)
    print('area under A(x) =', AX_a)

    fig_rho, ax_rho = plt.subplots()
    ax_rho.plot(x_in, AX_f, c='purple')
    ax_rho.tick_params(axis='both', which='major', labelsize=20)
    ax_rho.set_ylabel("$A(x)$", fontsize=18)
    ax_rho.set_xlabel("$x$", fontsize=18)
    ax_rho.axhline(y=0, c='k')
    ax_rho.axhline(y=AX0, alpha=0.5, c='k', linestyle='--')
    ax_rho.axvline(x=x0, alpha=0.5, c='k', linestyle='--')

    fig_rho.tight_layout()
    fig_rho.savefig('rho_func.pdf', dpi=720,
                    bbox_inches='tight')
    input('done?')
    # import sys
    # sys.exit()
    plt.close()


def initialise_res_dict():
    """Initialise result dictionary with none values.

    """
    result_dict = pd.DataFrame(
        columns=['prefix', 'rg', 'kappa_ang', 'x0',
                 'func', 'ds', 'w', 'vol', 'vq_si',
                 'ax0', 'Q', 'Qi']
    )
    return result_dict


def update_result_dict_file(result_file, result_dict, func, i, ds, Rg,
                            kappa_ang, x0, w, Vg, Vq_si, AX0,
                            tot_charge, num_vol_int):
    """Update result dictionary and output file with values.

    """
    result_dict['prefix'] = ['cts_'+str(func)+'_'+str(i)]
    result_dict['rg'] = Rg
    result_dict['kappa_ang'] = kappa_ang
    result_dict['x0'] = x0
    result_dict['func'] = func
    result_dict['ds'] = ds
    result_dict['w'] = w
    result_dict['vol'] = Vg
    result_dict['vq_si'] = Vq_si
    result_dict['ax0'] = AX0
    result_dict['Q'] = tot_charge
    result_dict['Qi'] = num_vol_int
    with open(result_file, 'a') as f:
        to_write = result_dict.iloc[0]
        f.write(str(to_write['prefix'])+',')
        f.write(str(to_write['rg'])+',')
        f.write(str(to_write['kappa_ang'])+',')
        f.write(str(to_write['x0'])+',')
        f.write(str(to_write['func'])+',')
        f.write(str(to_write['ds'])+',')
        f.write(str(to_write['w'])+',')
        f.write(str(to_write['vol'])+',')
        f.write(str(to_write['vq_si'])+',')
        f.write(str(to_write['ax0'])+',')
        f.write(str(to_write['Q'])+',')
        f.write(str(to_write['Qi']))
        f.write('\n')


def output_cts_data(prefix, solution, r_out, x_out, KB, sol_prop, e):
    """output cts data for this DS and func value to two files.

    """
    # output reduced potential and derivatives
    # as a function of reduced radial coord
    outfile = prefix+'_red_data.csv'
    psir = solution.sol(x_out)[0]
    # first derivative of spline
    dpsir = solution.sol.derivative(nu=1)(x_out)[0]
    # second derivative of spline
    d2psir = solution.sol.derivative(nu=2)(x_out)[0]
    with open(outfile, 'w') as f:
        f.write('x,phi,dphi,d2phi\n')
        for dat in np.arange(len(x_out)):
            f.write(str(x_out[dat])+',')
            f.write(str(psir[dat])+',')
            f.write(str(dpsir[dat])+',')
            f.write(str(d2psir[dat])+'\n')

    # output SI potential [mV] as a function of r [angstrom]
    outfile = prefix+'_si_data.csv'
    psi = psir * 1000 * KB * sol_prop['T'] / e
    with open(outfile, 'w') as f:
        f.write('r,psi\n')
        for dat in np.arange(len(r_out)):
            f.write(str(r_out[dat])+',')
            f.write(str(psi[dat])+'\n')
