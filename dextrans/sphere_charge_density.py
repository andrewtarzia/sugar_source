#!/usr/bin/env python

"""
Calculate + plot charge properties of ion-permeable sphere model of
functionalised carbohydrates.

Author: Andrew Tarzia

"""

import numpy as np
from scipy import constants
import matplotlib.pyplot as plt
import expressions_sphere_script as exp_s
import out_fun as pf
import parameters

# constants
KB = constants.Boltzmann  # boltzmann constant [J/K]
NA = constants.N_A  # avogadros number [mol-1]
e = constants.elementary_charge  # elementary charge [C]
E_0 = constants.epsilon_0  # Vacuum permittivity [F/m = C^2/(J.m)]
M_TO_NM = 1.0e9  # conversion from m to nm
M3_TO_L = 1.0e3  # conversion from m^3 to L
PI = constants.pi

if __name__ == '__main__':
    fig = plt.figure()
    ax1 = plt.subplot()
    ax1.tick_params(axis='both', which='major', labelsize=20)
    print('get polymer propeties')
    Rg, Vg, DS, DP, Lc = parameters.polymer_prop()
    print('get solution propeties')
    sol_prop = parameters.solution_properties()
    print('get functionalisation properties')
    func_colors, func_charges, func_pkas, w = parameters.func_prop()
    func_count = np.asarray(DS) * DP
    func_total_charges = {}
    for func in func_charges.keys():
        pka = func_pkas[func]
        charge = func_charges[func]
        partial_charge = exp_s.calculate_func_charge(
            charge,
            sol_prop['PH'],
            pka
        )
        func_total_charges[func] = func_count * partial_charge
    pf.plot_charge_properties('spherical_tc.pdf',
                              'spherical_vol_charge_density.pdf',
                              func_charges=func_charges,
                              func_colors=func_colors,
                              func_total_charges=func_total_charges,
                              Vg=Vg, DS=DS)
