#!/usr/bin/env python

"""
Functions for plotting.

Author: Andrew Tarzia

"""

import matplotlib.pyplot as plt


def function_of_sulphation(tc_name, lc_density_name,
                           sc_density_name, data):
    """
    Plot charge properties in data.

    """

    fig, ax1 = plt.subplots(figsize=(8, 6))
    for polymer in data:
        p_d = data[polymer]
        ax1.scatter(
            p_d[2],
            p_d[3],
            c=p_d[0],
            marker=p_d[1],
            edgecolor='k',
            label=p_d[6],
            s=100,
        )

    ax1.tick_params(axis='both', which='major', labelsize=20)
    ax1.set_xlabel("degree of sulphation", fontsize=18)
    ax1.set_ylabel("total charge [e]", fontsize=18)
    # ax1.axvline(x=0.125, c='k', lw=2)
    ax1.set_xlim(-0.1, 3.2)
    # ax1.set_ylim(-65, 65)

    ax1.legend(fancybox=True, ncol=1, fontsize=16)
    fig.tight_layout()
    fig.savefig(
        tc_name,
        dpi=720,
        bbox_inches='tight'
    )

    fig, ax1 = plt.subplots(figsize=(8, 6))
    ax1.tick_params(axis='both', which='major', labelsize=20)
    for polymer in data:
        p_d = data[polymer]
        ax1.scatter(
            p_d[2],
            p_d[4],
            c=p_d[0],
            marker=p_d[1],
            edgecolor='k',
            label=p_d[6],
            s=100,
        )

    ax1.tick_params(axis='both', which='major', labelsize=20)
    ax1.set_xlabel("degree of sulphation", fontsize=18)
    ax1.set_ylabel(
        r'line charge density [unitless]',
        fontsize=18
    )
    # ax1.axvline(x=0.125, c='k', lw=2)
    ax1.set_xlim(-0.1, 3.1)
    # ax1.set_ylim(-65, 65)

    ax1.legend(fancybox=True, ncol=1, fontsize=16)
    fig.tight_layout()
    fig.savefig(
        lc_density_name,
        dpi=720,
        bbox_inches='tight'
    )

    fig, ax1 = plt.subplots(figsize=(8, 6))
    for polymer in data:
        p_d = data[polymer]
        ax1.scatter(
            p_d[2],
            p_d[5],
            c=p_d[0],
            marker=p_d[1],
            edgecolor='k',
            label=p_d[6],
            s=100,
        )

    ax1.tick_params(axis='both', which='major', labelsize=20)
    ax1.set_xlabel("degree of sulphation", fontsize=18)
    ax1.set_ylabel(
        r"surface charge density [e/$\mathrm{\AA}^2$]",
        fontsize=18
    )
    # ax1.axvline(x=0.125, c='k', lw=2)
    ax1.set_xlim(-0.1, 3.2)
    # ax1.set_ylim(-65, 65)

    ax1.legend(fancybox=True, ncol=1, fontsize=16)
    fig.tight_layout()
    fig.savefig(
        sc_density_name,
        dpi=720,
        bbox_inches='tight'
    )
