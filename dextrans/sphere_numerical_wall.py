#!/usr/bin/env python

"""
Replicate the results in:
    Numerical Solution to the Poisson‐Boltzmann Equation for Spherical
    Polyelectrolyte Molecules

Author: Andrew Tarzia

"""

import numpy as np
import pandas as pd
# replace matlab BVP solver with scipy
from scipy import integrate
from scipy import constants
import out_fun as pf


# constants
KB = constants.Boltzmann  # boltzmann constant [J/K]
NA = constants.N_A  # avogadros number [mol-1]
e = constants.elementary_charge  # elementary charge [C]
E_0 = constants.epsilon_0  # Vacuum permittivity [F/m = C^2/(J.m)]
M_TO_NM = 1.0e9  # conversion from m to nm
M3_TO_L = 1.0e3  # conversion from m^3 to L
PI = constants.pi

# solution properties
# set temp
T = 298  # [K]
# set relative permittivity [unit less] -- water
E_r = 78.46
# set pH of solution
PH = 11
# salt concs (1 = Zn2+, 2 = acetate1-)
salt1_conc = 0.04  # mol/L
salt2_conc = 0.04  # mol/L
# bulk salt conc (total)
salt_conc = salt1_conc + salt2_conc  # mol/L
# ion valencies
Z1 = 1  # zinc 2+
Z2 = -1  # co-ion

# variables
x0 = 3
AX0 = 20
w = 0.1


def fun_red(xx, y):
    """Poisson-Boltzmann equation in spherical coordinates.
    Spherical - reduced coordinates
    reduced parametrs: psi(r) = PSI

    y(0)  = psi(r)
    y(0)' = dpsi / dr = y(2) / r^2
    y(1)  = r^2 (dpsi / dr) = r^2 * y(1)
    y(1)' = r^2 ( (-e / epsilon)[sum(zi ni exp(-zi*psi(r)))]
                - (rho(r) / epsilon)  )

    rr in m
    everything in SI
    n+, n- in: m^-3

    """
    global AX0
    global x0
    global w

    # print(z_m, z_p, n_p, n_m, epsilon, vg, rg, KB, T, e)

#    ax = np.zeros(xx.size)
#    ax[xx <= x0] = AX0
    # calculate smooth function AX_f to avoid numerical issues
    AX_f = -(AX0/2) * (np.tanh((xx - x0) / (2 * w)) - 1)
    inner = (np.exp(y[0]) - np.exp(-y[0]))

    # y1 = phi, y2 = dphi/dx
    dydr = np.vstack([y[1],
                      inner - AX_f - 2 * y[1]/xx])

    return dydr


def bc(ya, yb):
    """BCs for Poisson-Boltzmann equation in cylindrical coords.
    Boundary conditions for Poisson-Boltzmann equation in cylindrical
    coordinates in reduced units:
      y(1) = psir = z*e/(k*T)*psi
      y(2) = rr*dpsir/drr  (rr = r/radius)
      y(2) = 0 at r=0
      y(2) = z*e*sig*radius/(eps0*eps*k*T) = sigr

    redefined:
        if:
          y = psir = z*e/(k*T)*psi
        BCs:
          y' = 0 at r=0
          y  = 0 at r=R

    notes:
        ya[0] implies the function y at first boundary a
        ya[1] implies the function y' at first boundary a
        yb[0] implies the function y at second boundary b
        yb[1] implies the function y' at second boundary b

    """

    res = [ya[1],
           yb[0]]
    return res


def pb_cylinder_init(rr, inner, outer, inner_d1, outer_d1,
                     inner_d2, outer_d2, A):
    """Initial guess for solution to PB eqn in spherical coords.

    initial mesh:
        [from analytical solution Ohshima, 93]
        at r == 0 (y[0, 0]),
                potential = rho/(eps_0 * eps_r * kappa ** 2)
             == e * rho / (kb * T * eps_0 * eps_r * kappa ** 2)
             in reduced potential units
        at r = Rmax (y[0, 1]), potential == 0
    """

    yinit = np.zeros((2, rr.size))
    # y1 = phi, y2 = dphi/dx
    # yinit[0] = np.append(inner, outer)
    # yinit[1] = np.append(inner_d1, outer_d1)
    # y1 = phi, y2 = x^2 * dphi/dx
    # yinit[0] = np.append(inner, outer)
    # yinit[1] = np.append(inner_d1, outer_d1) * (rr ** 2)
    # manual set of y at x0
    yinit[0][0] = np.arcsinh(A/2)
    yinit[1][0] = 0

    return yinit


if __name__ == '__main__':
    # use the analytical solution by Ohshima as initial guess
    x_in = np.linspace(0.1, 6, 2)
    x_out = np.linspace(0.01, 6, 100)

    fig, ax1 = pf.initialise_wall_plot()

    # get intial solution
    # calculate analytical solution from Ohshima 93
    x_in_1 = x_in[x_in <= x0]
    x_in_2 = x_in[x_in > x0]

    solInit = pb_cylinder_init(x_in,
                               None, None, None, None, None, None,
                               AX0)
    y = solInit
    # print(y)

    # print('volume charge density =', AX, 'Rg =', x0)
    print('A(x = 0) =', AX0, 'X0 =', x0)

    # solve!
    # fun = RHS of system of ODEs
    # bc = boundary conditions
    # in_x = initial mesh
    # y = initial guess
    solution = integrate.solve_bvp(fun_red, bc, x_in, y,
                                   max_nodes=1000,
                                   verbose=False, tol=1E-5)
    print(solution.success, solution.message, solution.status)

    # print('plot residuals:')
    # print(solution.rms_residuals)
    # ax1.plot(solution.x[:-1], solution.rms_residuals,
    #          c='r')
    # ax1.axvline(x0,
    #             c='k', alpha=0.4)

    # print(solution.y[0])
    # reduced radial coord. (for output)
    # reduced electrostatic potential
    # output the values of sol for all r
    psir = solution.sol(x_out)[0]  # reduced

    # electrostatic potential [mV]
    psi = psir
    ax1.plot(x_out, psi, c='k')
    ax1.axvline(x0, c='k')

    # example data
    file = '/home/atarzia/psugars/working_dir/publication/example_data.csv'
    eg_data = pd.read_csv(file, skiprows=[0], names=['x', 'psi'])
    ax1.plot(eg_data.x, eg_data.psi, c='r')

    ax1.set_xlim(0, 6)
    ax1.set_ylim(0, 10)
    pf.finalise_wall_plot(fig, ax1,
                          'spherical_sum_wall.pdf')
