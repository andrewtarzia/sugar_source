#!/usr/bin/env python

"""
Functions for IO.

Author: Andrew Tarzia

"""

import pandas as pd
import numpy as np


def initialise_res_dict():
    """Initialise result dictionary with none values.

    """
    result_dict = pd.DataFrame(
        columns=['prefix', 'rg', 'kappa_ang', 'x0',
                 'func', 'ds', 'w', 'vol', 'vq_si',
                 'ax0', 'Q', 'Qi']
    )
    return result_dict


def update_result_dict_file(result_file, result_dict, func, i, ds, Rg,
                            kappa_ang, x0, w, Vg, Vq_si, AX0,
                            tot_charge, num_vol_int):
    """Update result dictionary and output file with values.

    """
    result_dict['prefix'] = ['cts_'+str(func)+'_'+str(i)]
    result_dict['rg'] = Rg
    result_dict['kappa_ang'] = kappa_ang
    result_dict['x0'] = x0
    result_dict['func'] = func
    result_dict['ds'] = ds
    result_dict['w'] = w
    result_dict['vol'] = Vg
    result_dict['vq_si'] = Vq_si
    result_dict['ax0'] = AX0
    result_dict['Q'] = tot_charge
    result_dict['Qi'] = num_vol_int
    with open(result_file, 'a') as f:
        to_write = result_dict.iloc[0]
        f.write(str(to_write['prefix'])+',')
        f.write(str(to_write['rg'])+',')
        f.write(str(to_write['kappa_ang'])+',')
        f.write(str(to_write['x0'])+',')
        f.write(str(to_write['func'])+',')
        f.write(str(to_write['ds'])+',')
        f.write(str(to_write['w'])+',')
        f.write(str(to_write['vol'])+',')
        f.write(str(to_write['vq_si'])+',')
        f.write(str(to_write['ax0'])+',')
        f.write(str(to_write['Q'])+',')
        f.write(str(to_write['Qi']))
        f.write('\n')


def output_cts_data(prefix, solution, r_out, x_out, KB, sol_prop, e):
    """output cts data for this DS and func value to two files.

    """
    # output reduced potential and derivatives
    # as a function of reduced radial coord
    outfile = prefix+'_red_data.csv'
    psir = solution.sol(x_out)[0]
    # first derivative of spline
    dpsir = solution.sol.derivative(nu=1)(x_out)[0]
    # second derivative of spline
    d2psir = solution.sol.derivative(nu=2)(x_out)[0]
    with open(outfile, 'w') as f:
        f.write('x,phi,dphi,d2phi\n')
        for dat in np.arange(len(x_out)):
            f.write(str(x_out[dat])+',')
            f.write(str(psir[dat])+',')
            f.write(str(dpsir[dat])+',')
            f.write(str(d2psir[dat])+'\n')

    # output SI potential [mV] as a function of r [angstrom]
    outfile = prefix+'_si_data.csv'
    psi = psir * 1000 * KB * sol_prop['T'] / e
    with open(outfile, 'w') as f:
        f.write('r,psi\n')
        for dat in np.arange(len(r_out)):
            f.write(str(r_out[dat])+',')
            f.write(str(psi[dat])+'\n')
