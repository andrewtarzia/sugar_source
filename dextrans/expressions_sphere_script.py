#!/usr/bin/env python

"""
Expressions used in numerical and analytical solution to PB
equation.

Author: Andrew Tarzia

"""

import numpy as np
from scipy import integrate
from sys import exit


def calculate_kappasq(e, I, e0, eR, KB, T, PI):
    """
    Calculate kappa^2 (== debye length ^ (-1/2)) in m^-2 (SI units)

    """
    top = 2 * (e ** 2) * I
    bottom = e0 * eR * KB * T

    kappasq = top / bottom

    return kappasq


def calculate_func_charge(charge, pH, pka):
    """
    Calculate the charge probability of a functional group with a given
    pKa for a given pH.

    charge must be 1 or -1.

    Code inspired by Biopython and this example:
        http://fields.scripps.edu/DTASelect/20010710-pI-Algorithm.pdf
    """
    if abs(charge) != 1:
        print('non unit charge given. Exiting...')
        exit()
    if charge == 1:
        # positive
        ratio = 10**(pka - pH) / (1 + 10**(pka - pH))
    else:
        # negative
        ratio = -10**(pH - pka) / (1 + 10**(pH - pka))

    return ratio


def calculate_conc(pot, bulk, val, e, KB, T):
    """
    Calculate the concentration of ion with bulk conc bulk and valency
    val at a position with potential pot in V using the Boltzman
    equation
    """
    B = (-val * e * pot) / (KB * T)
    conc = bulk * np.exp(B)
    return conc


def a_sol(Vg_q_si, E_0, E_r, kappa_si, kappa_a_si, r_in_1_si,
          r_in_2_si, Rg_si):
    """Calculate analytical solution on either side of Rg from:
        https://www.sciencedirect.com/science/
        article/pii/S0021979783710647

    Arguments:
        Vg_q_si (float) : volume charge density in C/m3
        E_0 (float) : vac. permittivity in F/m
        E_r (float) : relative permittivity
        kappa_si (float) : debye parameter in m^-1
        kappa_a_si (float) : debye parameter * Rg
        r_in_1_si (numpy array) : distance from centre of sphere
            in m (inside)
        r_in_2_si (numpy array) : distance from centre of sphere
            in m (outside)
        Rg_si (float) : radius of gyration in m

    """

    front = Vg_q_si / (E_0 * E_r * (kappa_si ** 2))

    inner_ = front * (1 - ((1 + (1/kappa_a_si)) * Rg_si * np.exp(-kappa_a_si) * (1/r_in_1_si) * np.sinh(kappa_si * r_in_1_si)))
    outer_ = front * (np.cosh(kappa_a_si) - (1/kappa_a_si) * np.sinh(kappa_a_si) ) * Rg_si * np.exp(-kappa_si * r_in_2_si) * (1/r_in_2_si)

    return inner_, outer_


def a_deriv(Vg_q_si, E_0, E_r, kappa_si, kappa_a_si, r_in_1_si,
            r_in_2_si, Rg_si):
    """Calculate d/dr of analytical solution on either side of Rg from:
        https://www.sciencedirect.com/science/article/pii/S0021979783710647

    Arguments:
        Vg_q_si (float) : volume charge density in C/m3
        E_0 (float) : vac. permittivity in F/m
        E_r (float) : relative permittivity
        kappa_si (float) : debye parameter in m^-1
        kappa_a_si (float) : debye parameter * Rg
        r_in_1_si (numpy array) : distance from centre of sphere
            in m (inside)
        r_in_2_si (numpy array) : distance from centre of sphere
            in m (outside)
        Rg_si (float) : radius of gyration in m

    """

    front = Vg_q_si / (E_0 * E_r * (kappa_si ** 2))

    front_inner = front * (1 + (1/kappa_a_si)) * Rg_si * np.exp(-kappa_a_si)
    front_outer = front * (np.cosh(kappa_a_si) - (1/kappa_a_si) * np.sinh(kappa_a_si) ) * Rg_si

    # inner_ = front_inner * ((-kappa_si/r_in_1_si) * np.cosh(kappa_si * r_in_1_si) + (1/(r_in_1_si ** 2)) * np.sinh(kappa_si * r_in_1_si))
    inner_ = (front_inner / (r_in_1_si ** 2)) * ((-kappa_si * r_in_1_si) * np.cosh(kappa_si * r_in_1_si) + np.sinh(kappa_si * r_in_1_si))
    outer_ = -front_outer * np.exp(-kappa_si * r_in_2_si) * ((1/(r_in_2_si ** 2)) + (kappa_si/r_in_2_si))

    return inner_, outer_


def a_deriv2(Vg_q_si, E_0, E_r, kappa_si, kappa_a_si, r_in_1_si,
             r_in_2_si, Rg_si):
    """Calculate d^2/dr^2 of analytical solution on either side of Rg from:
        https://www.sciencedirect.com/science/article/pii/S0021979783710647

    Arguments:
        Vg_q_si (float) : volume charge density in C/m3
        E_0 (float) : vac. permittivity in F/m
        E_r (float) : relative permittivity
        kappa_si (float) : debye parameter in m^-1
        kappa_a_si (float) : debye parameter * Rg
        r_in_1_si (numpy array) : distance from centre of sphere
            in m (inside)
        r_in_2_si (numpy array) : distance from centre of sphere
            in m (outside)
        Rg_si (float) : radius of gyration in m

    """

    front = Vg_q_si / (E_0 * E_r * (kappa_si ** 2))

    front_inner = front * (1 + (1/kappa_a_si)) * Rg_si * np.exp(-kappa_a_si)
    front_outer = front * (np.cosh(kappa_a_si) - (1/kappa_a_si) * np.sinh(kappa_a_si) ) * Rg_si

    # inner_ = front_inner * ((-2/(r_in_1_si ** 3)) * np.sinh(kappa_si * r_in_1_si) + (2 * kappa_si/(r_in_1_si ** 2)) * np.cosh(kappa_si * r_in_1_si) - ((kappa_si ** 2)/r_in_1_si) * np.sinh(kappa_si * r_in_1_si))
    inner_ = (front_inner / (r_in_1_si ** 3)) * ((2 * kappa_si * r_in_1_si) * np.cosh(kappa_si * r_in_1_si) - ((kappa_si ** 2) * (r_in_1_si ** 2) + 2) * np.sinh(kappa_si * r_in_1_si))
    outer_ = front_outer * np.exp(-kappa_si * r_in_2_si) * ((2/(r_in_2_si ** 3)) + (2 * kappa_si/(r_in_2_si ** 2)) + ((kappa_si ** 2)/r_in_2_si))

    return inner_, outer_


def a_sol_r(AX0, kappa_a_si, x_inner, x_outer):
    """Calculate analytical solution on either side of Rg from:
        https://www.sciencedirect.com/science/
        article/pii/S0021979783710647

    Arguments:
        AX0
        kappa_a_si
        x_inner
        x_outer

    """

    front = AX0/2

    inner_ = front * (1 - ((1 + (1/kappa_a_si)) * kappa_a_si * np.exp(-kappa_a_si) * (1/x_inner) * np.sinh(x_inner)))
    outer_ = front * (kappa_a_si * np.cosh(kappa_a_si) - np.sinh(kappa_a_si)) * (1/x_outer) * np.exp(-x_outer)

    return inner_, outer_


def a_deriv_r(AX0, kappa_a_si, x_inner, x_outer):
    """Calculate d/dr of analytical solution on either side of Rg from:
        https://www.sciencedirect.com/science/
        article/pii/S0021979783710647

    Arguments:
        AX0
        kappa_a_si
        x_inner
        x_outer

    """

    front = AX0/2

    inner_mid = front * (1 + (1/kappa_a_si)) * kappa_a_si * np.exp(-kappa_a_si)

    outer_mid = front * (kappa_a_si * np.cosh(kappa_a_si) - np.sinh(kappa_a_si))

    inner_ = inner_mid * ((1/(x_inner ** 2)) * np.sinh(x_inner) - (1/x_inner) * np.cosh(x_inner))
    outer_ = -outer_mid * np.exp(-x_outer) * ((x_outer ** -2) + (x_outer ** -1))

    return inner_, outer_


def a_deriv2_r(AX0, kappa_a_si, x_inner, x_outer):
    """Calculate d^2/dr^2 of analytical solution on either side of
    Rg from:
        https://www.sciencedirect.com/science/
        article/pii/S0021979783710647

    Arguments:
        AX0
        kappa_a_si
        x_inner
        x_outer

    """

    front = AX0/2

    inner_mid = front * (1 + (1/kappa_a_si)) * kappa_a_si * np.exp(-kappa_a_si)

    outer_mid = front * (kappa_a_si * np.cosh(kappa_a_si) - np.sinh(kappa_a_si))

    inner_ = inner_mid * ((-2 * (x_inner ** -3) * np.sinh(x_inner)) + (2 * (x_inner ** -2) * np.cosh(x_inner)) - ((x_inner ** -1) * np.sinh(x_inner)))
    outer_ = outer_mid * np.exp(-x_outer) * (2 * (x_outer ** -2) + 2 * (x_outer ** -2) + (x_outer ** -1))

    return inner_, outer_


def get_smooth_fn(AX0, x_in, x0, w):
    """Output the smooth with magnitude AX0, width w, transition at x0.

    """
    A_f = -(AX0/2) * (np.tanh((x_in - x0) / (2 * w)) - 1)

    return A_f


def get_Q_from_smooth_fn(AX_f, e, sol_prop, r_in, PI):
    """Get the total charge as the volume integral of the smooth function.

    """
    SI_AX_f = AX_f * e * sol_prop['I_m3']  # C / m^3
    SI_AX_f = SI_AX_f * (10 ** -30) / e  # e / angstrom^3
    AX_f_v_int = integrate.simps(SI_AX_f * (r_in ** 2), x=r_in) * 4 * PI
    return AX_f_v_int


def calc_three_integrands(AX0, x_in, x0, w, ds):
    """Calculate the three integrands of A(x).

    """
    AX_f = get_smooth_fn(AX0, x_in, x0, w)

    # numerical
    # integrate AX function using simpsons rule
    AX_i = integrate.simps(AX_f, x=x_in)

    # analytical integrand from x = x_min to x = x_max
    x_min = min(x_in)
    x_max = max(x_in)
    AX_i_analytic = -(AX0/2) * (
        (2 * w * np.log(np.cosh((x_max - x0) / (2 * w))) - x_max)
        - (2 * w * np.log(np.cosh((x_min - x0) / (2 * w))) - x_min)
    )

    # from area under the step function (discontinuous)
    AX_a = AX0 * x0

    return AX_f, AX_i, AX_i_analytic, AX_a
