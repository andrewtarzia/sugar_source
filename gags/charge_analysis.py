#!/usr/bin/env python

"""
Calculate + plot charge properties of cylindrical GAGs.

Author: Andrew Tarzia

"""

from scipy import constants
import plots as pf
import parameters

# constants
KB = constants.Boltzmann  # boltzmann constant [J/K]
NA = constants.N_A  # avogadros number [mol-1]
e = constants.elementary_charge  # elementary charge [C]
E_0 = constants.epsilon_0  # Vacuum permittivity [F/m = C^2/(J.m)]
M_TO_NM = 1.0e9  # conversion from m to nm
M3_TO_L = 1.0e3  # conversion from m^3 to L
PI = constants.pi


def get_sulph_charge_properties(poly):
    """
    Get the charge and line charge density as a function of sulphation.

    """
    sulphation = poly.sulphation
    t_charge = poly.get_charge()
    lc_density = poly.get_lc_density_ratio()
    sc_density = poly.get_surface_charge_density()

    return sulphation, t_charge, lc_density, sc_density


def main():
    # Iterate over polymers.
    polymers = ['HA', 'CS', 'HP', 'DS']
    polymer_markers = ('o', 'X', 'D', 'P')
    polymer_data = {}
    # MAF-7, ZIF-90, ZIF-8
    # pHs = [5.5, 7, 10]
    # pH_markers = ('o', 'X', 'D')
    for polymer, marker in zip(polymers, polymer_markers):
        polymer_data[polymer] = {}
        poly = parameters.Polymer(
            type=polymer,
            no_disacc=50,
            conc=0.36
        )
        colour = poly.colour
        # Assume fully ionized - so no impact of pH.
        # Get total charge and line charge density for each pH.
        # for pH, marker in zip(pHs, pH_markers):
        label = f"{poly.properties['figure_name']}"
        # Each polymer could have a different max_sulphation.
        # Therefore, new DS for each one.
        DS, t_c, lc_d, sc_d = get_sulph_charge_properties(poly=poly)
        polymer_data[polymer] = (
            colour,
            marker,
            DS,
            t_c,
            lc_d,
            sc_d,
            label
        )

    pf.function_of_sulphation(
        tc_name='fn_sulphation_tc.pdf',
        lc_density_name='fn_sulphation_lc_density.pdf',
        sc_density_name='fn_sulphation_sc_density.pdf',
        data=polymer_data
    )


if __name__ == '__main__':
    main()
