#!/usr/bin/env python

"""
Analyse output from sphere_numerical_generalized.py

Author: Andrew Tarzia

"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import constants
import expressions_sphere_script as exp_s
import parameters

# constants
KB = constants.Boltzmann  # boltzmann constant [J/K]
NA = constants.N_A  # avogadros number [mol-1]
e = constants.elementary_charge  # elementary charge [C]
E_0 = constants.epsilon_0  # Vacuum permittivity [F/m = C^2/(J.m)]
M_TO_NM = 1.0e9  # conversion from m to nm
M3_TO_L = 1.0e3  # conversion from m^3 to L
PI = constants.pi


def integ_vs_ds(res_df):
    """Plot integrands vs DS.

    """
    print('plot all integrands vs DS')
    func_colors, func_charges, func_pkas, w = parameters.func_prop()
    fig, ax1 = plt.subplots()
    for func in func_pkas.keys():
        df = res_df[res_df['func'] == func]
        ax1.plot(df['ds'], df['ax_i_n'],
                 c=func_colors[func], label=func+' - numerical', lw=2,
                 marker='o')
        ax1.plot(df['ds'], df['ax_i_a'],
                 c=func_colors[func], label=func+' - analytical', lw=2,
                 marker='x')
        ax1.plot(df['ds'], df['ax_i_area'],
                 c=func_colors[func], label=func+' - area', lw=2,
                 marker='D')

    ax1.tick_params(axis='both', which='major', labelsize=16)
    ax1.set_xlabel("DS", fontsize=16)
    ax1.set_ylabel("integrands", fontsize=16)
    ax1.set_ylim(-150, 150)

    ax1.legend(fancybox=True, ncol=2,
               fontsize=12)
    fig.tight_layout()
    fig.savefig('integ_vs_ds.pdf',
                dpi=720, bbox_inches='tight')
    plt.close()


def Q_vs_ds(res_df):
    """Plot difference between total charge and vol integral vs DS.

    """
    print('plot all integrands vs DS')
    func_colors, func_charges, func_pkas, w = parameters.func_prop()
    fig, ax1 = plt.subplots()
    for func in func_pkas.keys():
        df = res_df[res_df['func'] == func]
        ax1.plot(df['ds'], (df['Qi']-df['Q'])/df['Q'],
                 c=func_colors[func], label=func+' - numerical', lw=2,
                 marker='o')

    ax1.tick_params(axis='both', which='major', labelsize=16)
    ax1.set_xlabel("DS", fontsize=16)
    ax1.set_ylabel("(Q - $Q_i$)/Q", fontsize=16)
    ax1.set_ylim(-0.150, 0.150)

    ax1.legend(fancybox=True, ncol=2,
               fontsize=12)
    fig.tight_layout()
    fig.savefig('Qdiff_vs_ds.pdf',
                dpi=720, bbox_inches='tight')
    plt.close()


def plot_cf_reduced_potential(res_df, func, ylim, DS_c, colours):
    """Plot comparison of numerical and analytical solutions
    for a selection of DS values for a chosen functionality.

    """
    fig, ax1 = plt.subplots()
    df = res_df[res_df['func'] == func]
    count = 0
    for i, row in df.iterrows():
        if float(row['ds']) not in DS_c:
            continue
        # read reduced data file
        data = pd.read_csv(row['prefix']+'_red_data.csv')
        # plot numerical solution
        ax1.plot(
            data['x'], data['phi'],
            c=colours[count],
            label='numerical - DS = '+str(row['ds'])
        )
        # calculate and plot analytical solution
        x_in_1 = data['x'][data['x'] <= res_df['x0'].iloc[i]]
        x_in_2 = data['x'][data['x'] > res_df['x0'].iloc[i]]
        inner_, outer_ = exp_s.a_sol_r(res_df['ax0'].iloc[i],
                                       res_df['x0'].iloc[i],
                                       x_in_1, x_in_2)
        y_analy = np.append(inner_, outer_)
        ax1.plot(data['x'],
                 y_analy,
                 c=colours[count],
                 linestyle='--',
                 label='analytical - DS = '+str(row['ds']))
        count += 1

    ax1.legend(fancybox=True, ncol=1,
               fontsize=12)
    ax1.tick_params(axis='both', which='major', labelsize=16)
    ax1.set_xlabel(r"$\widetilde{r}$", fontsize=16)
    ax1.set_ylabel(r"$\widetilde{\psi}$", fontsize=16)
    ax1.set_ylim(ylim[0], ylim[1])
    ax1.set_xlim(0, max(data['x']))

    fig.tight_layout()
    fig.savefig('phi_vs_x_for_ds_'+func+'.pdf',
                dpi=720, bbox_inches='tight')
    plt.close()


def plot_cf_potential(res_df, func, ylim, DS_c, colours):
    """Plot comparison of numerical and analytical solutions
    for a selection of DS values for a chosen functionality.

    """
    sol_prop = parameters.solution_properties(verbose=False)
    fig, ax1 = plt.subplots()
    df = res_df[res_df['func'] == func]
    count = 0
    for i, row in df.iterrows():
        if float(row['ds']) not in DS_c:
            continue
        # read reduced data file
        data = pd.read_csv(row['prefix']+'_red_data.csv')
        # convert to SI units
        X = data['x'] / float(row['kappa_ang'])
        Y = data['phi'] * 1000 * KB * sol_prop['T'] / e
        # plot numerical solution
        ax1.plot(X, Y,
                 c=colours[count],
                 label='numerical - DS = '+str(row['ds']))
        # calculate and plot analytical solution
        x_in_1 = data['x'][data['x'] <= res_df['x0'].iloc[i]]
        x_in_2 = data['x'][data['x'] > res_df['x0'].iloc[i]]
        inner_, outer_ = exp_s.a_sol_r(res_df['ax0'].iloc[i],
                                       res_df['x0'].iloc[i],
                                       x_in_1, x_in_2)
        y_analy = np.append(inner_, outer_)
        # convert to SI units
        y_analy = y_analy * 1000 * KB * sol_prop['T'] / e
        # plot analytical solution
        ax1.plot(X,
                 y_analy,
                 c=colours[count],
                 linestyle='--',
                 label='analytical - DS = '+str(row['ds']))
        count += 1

    ax1.legend(fancybox=True, ncol=1,
               fontsize=12)
    ax1.tick_params(axis='both', which='major', labelsize=16)
    # ax1.set_xlabel("$\widetilde{r}$", fontsize=16)
    # ax1.set_ylabel("$\widetilde{\psi}$", fontsize=16)
    ax1.set_xlabel(r"$r$ $[\mathrm{\AA}]$", fontsize=16)
    ax1.set_ylabel("electrostatic potential [mV]", fontsize=16)
    ax1.set_ylim(ylim[0], ylim[1])
    ax1.set_xlim(0, 100)

    fig.tight_layout()
    fig.savefig('phi_vs_x_for_ds_'+func+'.pdf',
                dpi=720, bbox_inches='tight')
    plt.close()


def red_num_vs_anal(res_df, DS_c):
    """Plot comparison of analytical and numerical solution for all funcs

    """
    print(
        'plot reduced potential as a function of radial coord low DS'
        'compare to analytical solution'
    )
    func_colors, func_charges, func_pkas, w = parameters.func_prop()

    colors = ['k', 'r', 'b', 'green']
    plot_cf_reduced_potential(res_df, 'carboxymethyl', (-2, 0.5),
                              DS_c, colors)
    plot_cf_reduced_potential(res_df, 'amine', (-0.5, 1),
                              DS_c, colors)


def num_vs_anal(res_df, DS_c):
    """Plot comparison of analytical and numerical solution for all
    funcs

    """
    print('plot SI potential as a function of radial coord low DS')
    print('compare to analytical solution')
    func_colors, func_charges, func_pkas, w = parameters.func_prop()

    colors = ['k', 'r', 'b', 'green']
    plot_cf_potential(res_df, 'carboxymethyl', (-50, 5),
                      DS_c, colors)
    plot_cf_potential(res_df, 'amine', (-5, 20),
                      DS_c, colors)


def pot_vs_ds(res_df, R):
    """Plot potential vs DS.

    """
    print('plot all potentials at some value R vs DS')
    func_colors, func_charges, func_pkas, w = parameters.func_prop()
    fig, ax1 = plt.subplots()
    for func in func_pkas.keys():
        df = res_df[res_df['func'] == func]
        to_plot_x = []
        to_plot_y = []
        for i, row in df.iterrows():
            # read potential data file
            data = pd.read_csv(row['prefix']+'_si_data.csv')
            closest_r = float(data['r'][
                np.isclose(data['r'], R, rtol=0, atol=0.05)
            ])
            pot_at_r = float(data['psi'][data['r'] == closest_r])
            to_plot_x.append(df['ds'].loc[i])
            to_plot_y.append(pot_at_r)
        ax1.plot(to_plot_x, to_plot_y,
                 c=func_colors[func],
                 lw=3)

    if func_colors is not None:
        dec = np.linspace(-1000, -999, 2)
        for func in func_colors:
            ax1.plot(dec, dec, c=func_colors[func],
                     label=func, lw=3)

    ax1.tick_params(axis='both', which='major', labelsize=16)
    # ax1.set_ylabel("$\psi$("+str(R)+") [mV]", fontsize=16)
    ax1.set_ylabel("electrostatic potential [mV]", fontsize=16)
    ax1.set_xlabel("degree of substitution", fontsize=16)
    ax1.set_xlim(min(res_df['ds']), max(res_df['ds']))
    ax1.set_ylim(-100, 100)
    ax1.axvline(x=0.125, c='k', lw=2)
    ax1.legend(fancybox=True, ncol=2,
               fontsize=12)
    fig.tight_layout()
    fig.savefig('pot_vs_ds.pdf',
                dpi=720, bbox_inches='tight')
    plt.close()


def enh_vs_ds(res_df, R):
    """Plot potential vs DS.

    """
    print('plot all enhancements at some value R vs DS')
    func_colors, func_charges, func_pkas, w = parameters.func_prop()
    sol_prop = parameters.solution_properties(verbose=False)
    # set constant global parameters
    z_1 = sol_prop['z1']

    fig, ax1 = plt.subplots()
    for func in func_pkas.keys():
        df = res_df[res_df['func'] == func]
        to_plot_x = []
        to_plot_y = []
        for i, row in df.iterrows():
            # read potential data file
            data = pd.read_csv(row['prefix']+'_si_data.csv')
            closest_r = float(data['r'][
                np.isclose(data['r'], R, rtol=0, atol=0.05)
            ])
            pot_at_r = float(data['psi'][data['r'] == closest_r])
            # get zinc ion enhancement at surface from potential
            # input potential in V (psi / 1000)
            # input bulk conc in mol/L
            # therefore output is in mol/L
            conc_at_r = exp_s.calculate_conc(pot_at_r/1000,
                                             sol_prop['c1'], z_1,
                                             e, KB,
                                             sol_prop['T'])
            enh_at_r = conc_at_r/sol_prop['c1']
            to_plot_x.append(df['ds'].loc[i])
            to_plot_y.append(enh_at_r)
        ax1.plot(to_plot_x, to_plot_y,
                 c=func_colors[func],
                 lw=3)

    if func_colors is not None:
        dec = np.linspace(-1000, -999, 2)
        for func in func_colors:
            ax1.plot(dec, dec, c=func_colors[func],
                     label=func, lw=3)

    ax1.tick_params(axis='both', which='major', labelsize=16)
    ax1.set_ylabel("Zn$^{2+}$ enhancement", fontsize=16)
    ax1.set_xlabel("degree of substitution", fontsize=16)
    ax1.set_xlim(min(res_df['ds']), max(res_df['ds']))
    ax1.set_ylim(0, 1000)
    ax1.set_yscale("symlog", nonposy='clip')
    ax1.axhline(y=10, c='k', lw=2, linestyle='--')
    ax1.axvline(x=0.125, c='k', lw=2)
    ax1.legend(fancybox=True, ncol=2,
               fontsize=12)
    fig.tight_layout()
    fig.savefig('enh_vs_ds.pdf',
                dpi=720, bbox_inches='tight')
    plt.close()


def pot_vs_r(res_df, ds):
    """Plot potential vs DS.

    """
    print('plot all enhancements at some value DS vs R')
    func_colors, func_charges, func_pkas, w = parameters.func_prop()
    # set constant global parameters
    fig, ax1 = plt.subplots()
    for func in func_pkas.keys():
        df = res_df[res_df['func'] == func]
        for i, row in df.iterrows():
            if float(row['ds']) != ds:
                continue
            # read potential data file
            data = pd.read_csv(row['prefix']+'_si_data.csv')
            pot = data.psi
            ax1.plot(data.r, pot,
                     c=func_colors[func],
                     lw=3)

    if func_colors is not None:
        dec = np.linspace(-1000, -999, 2)
        for func in func_colors:
            ax1.plot(dec, dec, c=func_colors[func],
                     label=func, lw=3)

    ax1.tick_params(axis='both', which='major', labelsize=16)
    ax1.set_ylabel("electrostatic potential [mV]", fontsize=16)
    ax1.set_xlabel(r"$r$ $[\mathrm{\AA}]$", fontsize=16)
    ax1.set_xlim(0, 100)
    ax1.set_ylim(-100, 100)
    ax1.axvline(x=13.9)
    ax1.axvline(x=0.125, c='k', lw=2)
    ax1.legend(fancybox=True, ncol=2,
               fontsize=12)
    fig.tight_layout()
    fig.savefig('pot_vs_r.pdf',
                dpi=720, bbox_inches='tight')
    plt.close()


def enh_vs_r(res_df, ds):
    """Plot potential vs DS.

    """
    print('plot all enhancements at some value DS vs R')
    func_colors, func_charges, func_pkas, w = parameters.func_prop()
    sol_prop = parameters.solution_properties(verbose=False)
    # set constant global parameters
    z_1 = sol_prop['z1']

    fig, ax1 = plt.subplots()
    for func in func_pkas.keys():
        df = res_df[res_df['func'] == func]
        for i, row in df.iterrows():
            if float(row['ds']) != ds:
                continue
            # read potential data file
            data = pd.read_csv(row['prefix']+'_si_data.csv')
            pot = data.psi
            # get zinc ion enhancement at surface from potential
            # input potential in V (psi / 1000)
            # input bulk conc in mol/L
            # therefore output is in mol/L
            conc = exp_s.calculate_conc(pot/1000,
                                        sol_prop['c1'], z_1,
                                        e, KB,
                                        sol_prop['T'])
            enh = conc/sol_prop['c1']
            ax1.plot(data.r, enh,
                     c=func_colors[func],
                     lw=3)

    if func_colors is not None:
        dec = np.linspace(-1000, -999, 2)
        for func in func_colors:
            ax1.plot(dec, dec, c=func_colors[func],
                     label=func, lw=3)

    ax1.tick_params(axis='both', which='major', labelsize=16)
    ax1.set_ylabel("Zn$^{2+}$ enhancement", fontsize=16)
    ax1.set_xlabel(r"$r$ $[\mathrm{\AA}]$", fontsize=16)
    ax1.set_xlim(0, 100)
    ax1.set_ylim(0, 15)
    ax1.axvline(x=13.9)
    # ax1.set_yscale("symlog", nonposy='clip')
    ax1.axhline(y=10, c='k', lw=2, linestyle='--')
    ax1.axvline(x=0.125, c='k', lw=2)
    ax1.legend(fancybox=True, ncol=2,
               fontsize=12)
    fig.tight_layout()
    fig.savefig('enh_vs_r.pdf',
                dpi=720, bbox_inches='tight')
    plt.close()


def diff_plot(res_df, DS_c):
    """Plot difference of LHS and RHS after plugging in solution.

    """
    print(
        'plot LHS-RHS after plugging numerical reduced potential '
        'into PB'
    )
    func_colors, func_charges, func_pkas, w = parameters.func_prop()
    sol_prop = parameters.solution_properties(verbose=False)
    # set constant global parameters
    n_1 = sol_prop['c1'] * NA * M3_TO_L  # m^-3
    n_2 = sol_prop['c2'] * NA * M3_TO_L  # m^-3
    z_1 = sol_prop['z1']
    z_2 = sol_prop['z2']
    I_m3 = sol_prop['I_m3']

    for func in func_pkas.keys():
        df = res_df[res_df['func'] == func]
        for i, row in df.iterrows():
            fig, ax1 = plt.subplots()
            if float(row['ds']) not in DS_c:
                continue
            # read reduced data file
            data = pd.read_csv(row['prefix']+'_red_data.csv')
            # calculate LHS
            LHS = data['d2phi'] + (2 / data['x']) * data['dphi']

            # calculate RHS
            AX_f = -(row['ax0']/2) * (np.tanh((data['x'] - row['x0']) / (2 * row['w'])) - 1)
            inner = (z_1 * n_1 * np.exp(-z_1 * data['phi']) + z_2 * n_2 * np.exp(-z_2 * data['phi']))
            RHS = (-inner / (2 * I_m3)) - (1 / 2) * AX_f

            # plot difference
            ax1.plot(data['x'], LHS-RHS, c='k', label="LHS-RHS",
                     linestyle='-')

            ax1.legend()
            ax1.tick_params(axis='both', which='major', labelsize=20)
            ax1.set_ylabel("LHS - RHS", fontsize=18)
            ax1.set_xlabel(r"$\widetilde{r}$", fontsize=18)
            ax1.set_title('DS = '+str(row['ds']))
            ax1.set_ylim(-1, 1)

            fig.tight_layout()
            fig.savefig(row['prefix']+'_diff.pdf',
                        dpi=720, bbox_inches='tight')
            plt.close()


if __name__ == '__main__':
    result_file = 'results.csv'
    res_df = pd.read_csv(result_file)

    # plotting functions
    Q_vs_ds(res_df)
    num_vs_anal(res_df, [0.01, 0.04, 0.06, 0.1])
    pot_vs_ds(res_df, 0.1)
    enh_vs_ds(res_df, 0.1)
    pot_vs_r(res_df, ds=0.125)
    enh_vs_r(res_df, ds=0.125)
    diff_plot(res_df, [0.01, 0.1, 0.5, 1.0])
