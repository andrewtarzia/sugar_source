import numpy as np
import scipy.special as sc

# constants
T = 298 # [K]
KB = 1.38064852E-23 # boltzmann constant [J/K]
NA = 6.022140857E23 # avogadros number [mol-1]
e = 1.60217662E-19 # elementary charge [C]
E_r = 78.46 # relative permittivity [unit less] -- water!
E_0 = 8.854E-12 # vacuum permittivity [CV^-1m^-1]
PI = np.pi

def calculate_bjerrum_length(Z):
    """
    Calculate the Bjerrum length for Z valent ion in water from Equation 1.3.
    
    Z = valency of ion [zinc = 2+]
    
    returns lb in angstrom
    """
    #top = (Z * e) ** 2
    top = (e) ** 2
    bottom = 4 * PI * E_0 * E_r * KB * T
    #bottom = E_0 * E_r * KB * T
    
    lb = top / bottom
    lb = lb * (10 ** 10) # to angstrom
    return lb # angstrom  

def calculate_kappasq(lb, I):
    """
    Calculate the inverse Debye screening parameter from:
    https://arxiv.org/abs/cond-mat/9701067
    
    lb - bjerrum length (m)
    I - ionic strength (molec/m3)
    """
    kappasq = 8 * PI * lb * I
    return kappasq


def calculate_y(zeta, kr, lamb):
    """
    Calculate the reduced potential (y) from E2.1 https://arxiv.org/abs/cond-mat/9701067 and https://onlinelibrary-wiley-com.proxy.library.adelaide.edu.au/doi/abs/10.1002/bip.360230209
    as a function of b (distance between charges)
    
    zeta - dimensionless ratio (array)
    kr - reduced distance from cylinder surface (array)
    lamb - line charge density
    """
    # use modified bessel function K0:
    # zeroth order of the second kind (n = 0)
    
    # if the cylinder is positively charged then the BCs change. 
    # We implement that here as multiplying y by -1 if lamb > 0
    ys = []
    for i, b in enumerate(zeta):
        B = sc.kn(0, kr)
        if lamb[i] > 0:
            y = 2 * b * B
        else:
            y = -2 * b * B
        ys.append(y)
    return ys
    
def calculate_conc(pot, bulk, val):
    """
    Calculate the concentration of ion with bulk conc bulk and valency val at a position with potential pot in V using the Boltzman equation
    """
    B = (-val * e * pot) / (KB * T)
    conc = bulk * np.exp(B)
    return conc

def calculate_reduced_conc(pot, bulk, Z):
    """
    Calculate the concentration of ion with bulk conc bulk using the Boltzman equation in reduced coords
    """
    if Z > 0:
        conc = bulk * np.exp(-pot)
    else:
        conc = bulk * np.exp(pot)
    return conc

### Using DMH equations for Manning parameter:
def calculate_sigma_of_cylinder(charge, length, radius):
    """
    Calculate surface charge density of cylinder as total charge / surface area of cylinder (Equation 1.7)
    
    charge [e [C]]
    length of cylinder [angstrom]
    radius of cylinder [angstrom]
    
    returns sigma [Cm^-2]
    
    """
    length = length * (10 ** -10) # to m
    radius = radius * (10 ** -10) # to m
    charge = e * charge # to C
    
    area = 2 * np.pi * radius * length + 2 * np.pi * (radius ** 2)
        
    sigma = charge / area
    return sigma # Cm^-2

def calculate_func_charge(charge, pH, pka):
    """
    Calculate the charge probability of a functional group with a given pKa for a given pH.
    """
    ratio = 1 / (1 + 10**(pH - pka))

    if charge == 1:
        return ratio
    else:
        return ratio - 1
    
def calculate_lambda(a, sigma):
    """
    Calculate the line charge density of a cylinder from Equation 1.4
    
    a = radius of cylinder [angstrom]
    sigma = surface charge density [Cm^-2]
    
    returns lambda in Cm^-1
    """
    a = a * (10 ** -10) # to m
    lamb = 2 * np.pi * a * sigma
    return lamb # in Cm^-1

def calculate_manning_parameter(lamb, lb, Z):
    """
    Calculate the manning parameter from Equation 1.5
        
    lamb = lambda = line charge density [Cm^-1]
    lb = bjerrum length [angstrom]
    Z = valency of ion [zinc = 2+]
    
    returns zeta as a unit less value
    
    """
    lb = lb * (10 ** -10) # to m
    
    top = abs(lamb) * lb # in Cm-1 * m = C
    #bottom = Z * e # in C
    bottom = e # in C
    
    zeta = top / bottom
    
    return zeta

def calculate_zinc_surface_conc(zeta, lb, a):
    """
    Calculate the zinc ion concentration at the surface of the carbohydrate from Equation 1.6
    
    zeta = manning parameter [unit less]
    lb = bjerrum length [angstrom]
    a = radius of cylinder [angstrom]
    
    returns n_plus(a) in mol/L
    
    """
    a = a * (10 ** -10) # to m
    lb = lb * (10 ** -10) # to m
    top = (1 - zeta) ** 2
    bottom = (2 * np.pi * lb * (a ** 2))
    n_plus = top / bottom # in m^-3
    n_plus = n_plus / 1000 # to L^-1
    n_plus = n_plus / NA  # to mol/L
    return n_plus