#!/usr/bin/env python

"""
Plot the smooth function based on test parameters

Author: Andrew Tarzia

"""

import numpy as np
import matplotlib.pyplot as plt


def main():
    fig, ax1 = plt.subplots()
    x = np.linspace(0, 15, 1000)
    AX = 5
    ws = [0.01, 0.02, 0.1, 0.5]
    cs = ['k', 'purple', 'green', 'orange']
    x0 = 3
    for i, w in enumerate(ws):
        # plot smooth func
        y = -(AX/2) * (np.tanh((x - x0) / (2 * w)) - 1)
        ax1.plot(x, y, c=cs[i], label="$w$ = "+str(w),
                 linestyle='-')

    ax1.legend(loc=1, fontsize=16)
    ax1.tick_params(axis='both', which='major', labelsize=20)
    # ax1.set_ylabel(r"$\widetilde{\rho}_s(\widetilde{r})$",
    # fontsize=18)
    # ax1.set_xlabel("$\widetilde{r}$", fontsize=18)
    ax1.set_ylabel(r"$\rho_s(r)$ [e/$\mathrm{\AA}^3$]", fontsize=18)
    ax1.set_xlabel(r"$r$ $[\mathrm{\AA}]$", fontsize=18)
    ax1.set_ylim(-1, 6)
    ax1.set_xlim(0, 15)
    ax1.axhline(y=AX, c='k', alpha=0.4)
    ax1.axvline(x=x0, c='k', alpha=0.4)

    fig.tight_layout()
    fig.savefig('smooth_fn.pdf',
                dpi=720, bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    main()
