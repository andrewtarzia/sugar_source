#!/usr/bin/env python

"""
Calculate unit cell distance based on solution properties.

Author: Andrew Tarzia

"""

import parameters


def main():
    solutions = {
        'MAF7': {'pH': 5.5},
        'ZIF90': {'pH': 7},
        'ZIF8': {'pH': 10},
    }
    # Get debye length of solution.
    for solution in solutions:
        sol = parameters.Solution(
            pH=solutions[solution]['pH'],
            salt1_conc=0.08,
            salt2_conc=0.16,
            salt1_Z=2,
            salt2_Z=-1
        )
        print(
            f'debye length of solution: {solution} = '
            f'{sol.debye_length} [angstrom]'
        )

        # Iterate over polymers.
        polymers = ['HA', 'CS', 'HP', 'DS']

        for polymer in polymers:
            poly = parameters.Polymer(
                type=polymer,
                no_disacc=50,
                conc=0.36
            )
            model = parameters.CellModel(
                polymer=poly,
                solution=sol,
                R_end=100,
            )
            model.calc_chain_distance()
            R_ang = model.R * (10 ** 10)
            print(
                f'separation distance = {R_ang} [angstrom] for '
                f'{polymer} -> R/debye_length = '
                f'{R_ang/sol.debye_length}.'
            )


if __name__ == '__main__':
    main()
