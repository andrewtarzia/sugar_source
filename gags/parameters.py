#!/usr/bin/env python

"""
Functions containing variables that describe models of GAGs.

Author: Andrew Tarzia

"""

import numpy as np
from scipy import constants


# Define constants.
# boltzmann constant [J/K]
KB = constants.Boltzmann
# avogadros number [mol-1]
NA = constants.N_A
# elementary charge [C]
e = constants.elementary_charge
# Vacuum permittivity [F/m = C^2/(J.m)]
E_0 = constants.epsilon_0
# conversion from m to nm
M_TO_NM = 1.0e9
# conversion from m^3 to L
M3_TO_L = 1.0e3
# PI
PI = constants.pi


class Polymer():
    """
    Defines a GAG polymer.

    Attributes
    ----------
    type : :class:`str`
        Type of polymer. Either HA, HP, CS, DS.

    properties : :class:`dict` of :class:`dicts`
        A series of polymer :attr:`type` specific polymer properties.

    no_disacc : :class:`int`
        Number of disaccharides in polymer chain.

    MW_per_disacc : :class:`float`
        Molecular weight (in g/mol) of disaccharide of polymer.
        Defined in :attr:`properties`.

    MW : :class:`float`
        Molecular weight of whole polymer chain (in g/mol).
        MW_per_disaccharide * no_disacc

    no_sug : :class:`int`
        Number of sugars in chain.
        no_disacc / 2

    chain_length : :class:`float`
        Length of polymer chain in Angstrom.
        no_disacc * properties['length_per_disacc']

    chain_radius : :class:`float`
        Radius of polymer chain in Angstrom.
        Assumed to be 5.5 Angstrom
        [https://pubs.acs.org/doi/full/10.1021/ma0106604].

    conc_gperL : :class:`float`
        Concentration of polymer in solution in units g/L.

    conc_gperm3 : :class:`float`
        Concentration of polymer in solution in units g/m3.
        conc_gperm3 = conc_gperL * 10^3

    colour : :class:`str`
        Colour to be used in plots for this polymer.
        Defined in :attr:`properties`.

    sulphation : :class:`float`
        Degree of sulphation of polymer. A set number for each GAG.
        Defined in :attr:`properties`.

    max_sulphation : :class:`float`
        Maximum degree of sulphation of polymer.
        A set number for each GAG.
        Defined in :attr:`properties`.

    intercharge_spacing : :class:`float`
        Intercharge along polymer chain assuming uniform distribution.
        In Angstrom.
        A set number for each GAG (CS == DS).
        Defined in :attr:`properties`.

    coo_per_disacc : :class:`float`
        Carboxylate functional groups (assumed to be points of charge)
        per disaccharide. A set number for each GAG.
        Defined in :attr:`properties`.

    """

    def __init__(self, type, no_disacc, conc):
        """
        Initialize a Polymer.

        Keyword Arguments
        -----------------
        type : :class:`str`
            Type of polymer. Either HA, HP, CS, DS.

        no_disacc : :class:`int`
            Number of disaaccharides in chain.

        conc : :class:`float`
            Polymer concentration in g/L

        """
        self.type = type
        self.properties = self.polymer_prop()[self.type]
        self.no_disacc = no_disacc
        self.define_chain()
        self.conc_gperL = conc
        self.conc_gperm3 = conc * (10 ** 3)
        self.colour = self.properties['colour']
        self.sulphation = self.properties['degree_of_sulphation']
        self.max_sulphation = self.properties['max_sulphation']
        self.intercharge_spacing = self.properties[
            'intercharge_spacing'
        ]
        self.coo_per_disacc = self.properties['coo_per_disacc']

    def define_chain(self):
        """
        Define the physical properties of the cylinder and polymer.

        """
        # g/mol.
        self.MW_per_disacc = self.properties['MW_per_disacc']
        self.MW = self.MW_per_disacc * self.no_disacc
        self.no_sug = self.no_disacc / 2
        # Angstrom.
        self.chain_length = self.no_disacc * self.properties[
            'length_per_disacc'
        ]
        # Standard assumption of sugar width in Angstrom.
        self.chain_radius = 5.5

    def get_charge(self):
        """
        Get polymer charge in units of e.

        """
        charge = 0
        # Avg. no. of carboxylates per disaaccharide.
        charge += self.no_disacc*self.coo_per_disacc*-1
        # Avg. no. of sulphates per disaaccharide.
        charge += self.no_disacc*self.sulphation*-1

        return charge

    def get_lc_density_ratio(self):
        """
        Get polymer line charge density as a unitless parameter.

        Defined as:
        epsilon = lambda_b (bjerrum length) / intercharge_spacing
        where,
        lambda_b = (e^2)/(4 * pi * epsilon_0*epsilon_r * k_B * T)

        """
        # Units: m
        lambda_b = (e ** 2) / (4 * PI * 80 * E_0 * KB * 298)
        # Units: Angstrom
        lambda_b = lambda_b * (10 ** 10)
        lc_density = lambda_b / self.intercharge_spacing
        return lc_density

    def get_lc_density(self):
        """
        Get polymer line charge density in e/Angstrom.

        Defined as:
        total charge in e / length of polymer chain in Angstrom.

        """
        top = self.get_charge()
        bottom = self.chain_length
        lc_density = top / bottom
        return lc_density

    def get_surface_charge_density(self):
        """
        Get polymer surface charge density of cylinder in e/Angstrom^2.

        Defined as:
        total charge in e / (
            2*pi*length of polymer chain in Angstrom*
            radius of cylinder in Angstrom
        ).

        """
        top = self.get_charge()
        bottom = 2 * PI * self.chain_length * self.chain_radius
        surface_charge_density = top / bottom
        return surface_charge_density

    def polymer_prop(self):
        """
        Define properties of 4 GAGs that are studied.

        """
        gags = {
            'HP':
                {
                    'figure_name': 'heparin',
                    'colour': '#FA7268',
                    'func_groups': [
                        'carboxylic_acid',
                        'sulphate'
                    ],
                    'coo_per_disacc': 1,
                    # Degree of sulphation: avg sulphates per disacc.
                    'max_sulphation': 3,
                    # Assume 3 sulphates.
                    'degree_of_sulphation': 3,
                    # C12 N H19 O20 S3 -- rounded
                    'MW_per_disacc': 600,
                    # Assume each sugar is approximately 10 Angstrom in
                    # length.
                    'length_per_disacc': 10,
                    # Approximate intercharge spacing in Angstrom.
                    # Approximately 10/3
                    'intercharge_spacing': 3.5
                },
            'HA':
                {
                    'figure_name': 'hyaluronic acid',
                    'colour': '#900C3F',
                    'func_groups': ['carboxylic_acid'],
                    'coo_per_disacc': 1,
                    # Degree of sulphation: avg sulphates per disacc.
                    'max_sulphation': 0,
                    'degree_of_sulphation': 0,
                    # C14 N H21 O11 -- rounded
                    'MW_per_disacc': 380,
                    # Assume each sugar is approximately 10 Angstrom in
                    # length.
                    'length_per_disacc': 10,
                    # Approximate intercharge spacing in Angstrom.
                    'intercharge_spacing': 10
                },
            'DS':
                {
                    'figure_name': 'chondroitin sulfate B',
                    'colour': '#DAF7A6',
                    'func_groups': [
                        'carboxylic_acid',
                        'sulphate'
                    ],
                    'coo_per_disacc': 1,
                    # Degree of sulphation: avg sulphates per disacc.
                    'max_sulphation': 1,
                    'degree_of_sulphation': 1.0,
                    # C14 N H21 O15 S -- rounded
                    'MW_per_disacc': 480,
                    # Assume each sugar is approximately 10 Angstrom in
                    # length.
                    'length_per_disacc': 10,
                    # Approximate intercharge spacing in Angstrom.
                    # From
                    # https://pubs.acs.org/doi/full/10.1021/ma0106604
                    'intercharge_spacing': 6.4
                },
            'CS':
                {
                    'figure_name': 'chondroitin sulfate C',
                    'colour': '#DB869D',
                    'func_groups': [
                        'carboxylic_acid',
                        'sulphate'
                    ],
                    'coo_per_disacc': 1,
                    # Degree of sulphation: avg sulphates per disacc.
                    'max_sulphation': 1,
                    'degree_of_sulphation': 1.0,
                    # C14 N H21 O15 S -- rounded
                    'MW_per_disacc': 480,
                    # Assume each sugar is approximately 10 Angstrom in
                    # length.
                    'length_per_disacc': 10,
                    # From
                    # https://pubs.acs.org/doi/full/10.1021/ma0106604
                    'intercharge_spacing': 6.4
                },
        }
        return gags


class Solution():
    """
    Defines an aqueous solution with some salt.

    The salt can be any valencies.

    Attributes
    ----------
    T : :class:`int`
        Temperature (K) - set to 298K, room temperature.

    E_r : :class:`int`
        Relative permittivity - set to 80, water.

    pH : :class:`float`
        pH of solution.

    salt1_conc : :class:`float`
        Concentration in mol/L of salt ion 1.

    salt2_conc : :class:`
        Concentration in mol/L of salt ion 2.

    salt1_Z : :class:`int`
        Valency of salt ion 1.

    salt2_Z : :class:`int`
        Valency of salt ion 2.

    I_mol : :class:`float`
        Ionic strength in units of mol/L.

    I_invm3 : :class:`float`
        Ionic strength in units of molecules/m^3.

    k2_invm2 : :class:`float`
        Kappa^2 in units of m^(-2).

    k_invm : :class:`float`
        Kappa in units of m^(-1).

    k_invang : :class:`float`
        Kappa in units of Angstrom^(-1).

    debye_length : :class:`float`
        The debye length (kappa^-1) in units of Angstrom.

    """

    def __init__(self, pH, salt1_conc, salt2_conc, salt1_Z, salt2_Z):
        # Set the temperature to 298 K.
        self.T = 298
        # Set relative permittivity [unit less] -- water.
        self.E_r = 80
        self.pH = pH
        self.salt1_conc = salt1_conc
        self.salt2_conc = salt2_conc
        self.salt1_Z = salt1_Z
        self.salt2_Z = salt2_Z
        self.calc_I()
        self.calc_debye()
        print("-------------------------------")
        print('I =', self.I_mol, 'mol/L')
        print('I =', self.I_invm3, 'm^-3')
        print("-------------------------------")
        print('kappa =', self.k_invang, 'angstrom^-1')
        print('debye length =',
              round(self.debye_length, 3), 'angstrom')
        print("-------------------------------")

    def calc_I(self):
        """
        Calculate ionic strength of solution.

        I = (1/2) * sum(z_i^2 * c_i)

        """
        # Calculate ionic strength.
        # This does not consider the [H+] and [OH-] due to pH.
        # Units in mol/L.
        salt1 = (self.salt1_conc * self.salt1_Z * self.salt1_Z)
        salt2 = (self.salt2_conc * self.salt2_Z * self.salt2_Z)
        self.I_mol = 0.5 * (salt1 + salt2)

        # In units of molec per m3.
        self.I_invm3 = NA * self.I_mol * M3_TO_L

    def calc_debye(self):
        """
        Calculate Kappa^2 and Debye length of solution.

        kappa^2 = debye length ^ (-1/2)

        """

        top = 2 * (e ** 2) * self.I_invm3
        bottom = E_0 * self.E_r * KB * self.T

        # m^-2
        self.k2_invm2 = top / bottom

        # m^-1
        self.k_invm = np.sqrt(self.k2_invm2)
        # Angstrom^-1
        self.k_invang = self.k_invm * (10 ** -10)
        # Debye length in Angstrom
        self.debye_length = 1/(self.k_invang)


class CellModel():
    """
    Defines the methods and properties of the cell model.

    Notes
    -----

    Based on cell model in:
    https://pubs.acs.org/doi/full/10.1021/ma0106604


    Attributes and Definitions
    --------------------------

    b_ang = intercharge distance [Angstrom]
    b_m = intercharge distance [m]
    a = radius of polymer [Angstrom]
    a_r = reduced radius of polymer [unitless]
    MW_per_disacc = molecular weight of an GAG disaccharide [g/mol]
    C_gag = GAG conc [g/m^3]
    R = calculated cell radius [m]
    R_end = implemented cell radius [Angstrom]
    sigma = surface charge density of cylinder [e/Angstrom^2]
    sigma_r = reduced surface charge density of cylinder [unitless]
    kappa = inverse debye length [angstrom]
    NA = avogadros number [mol-1]
    I_m3 = ionic strength of ions in solution [molecules/m3]
    n_i = concentration of ion i [XX]
    z_i = valency of ion i [unitless]

    r = radial distance [Angstrom]
    x = reduced radial distance [unitless]
    r_space = coordinates from r=a to r=R [Angstrom]
    x_space = reduced form of real space [unitless]

    psi = electrostatic potential [mV]
    phi = reduced electrostatic potential
        phi = (z * e / kB * T)* psi

    """

    def __init__(self, polymer, solution, R_end):
        """
        Initialize :class:`Model`.

        polymer : :class:`.Polymer`
            Polymer to be used in cell model.

        solution : :class:`.Solution`
            Solution surrounding polymer in cell model.

        R_end : :class:`float`
            Cell model radius [Angstrom].
            This should be checked to be much larger than the debye
            length and on the same order as R.

        """
        self.polymer = polymer
        self.solution = solution
        self.R_end = R_end
        self.b_ang = self.polymer.intercharge_spacing
        self.b_m = self.b_ang * (10 ** -10)
        self.a = self.polymer.chain_radius
        self.MW_per_disacc = self.polymer.MW_per_disacc
        self.C_gag = self.polymer.conc_gperm3
        self.sigma = self.polymer.get_surface_charge_density()
        self.reduce_sigma()
        self.kappa = solution.k_invang
        self.I_m3 = solution.I_invm3
        self.n_1 = solution.salt1_conc * NA * M3_TO_L
        self.z_1 = solution.salt1_Z
        self.n_2 = solution.salt2_conc * NA * M3_TO_L
        self.z_2 = solution.salt2_Z
        self.r_space = np.linspace(self.a, self.R_end, 1000)
        self.x_space = self.r_space * self.kappa
        self.a_r = self.a * self.kappa

    def unreduce_phi(self, values):
        """
        Convert phi [unitless] to psi [mV].

        Defined by:
        phi = psi * (e / kB * T)

        """
        return values * 1000 * KB * self.solution.T / e

    def calc_ion_concentrations(self, potential):
        """
        Calculate the concentration of an ion at some position.

        Defined as:
        c_i(r) = c_i, bulk * exp(-z_i * e * potential / k_B * T)

        """

        # Must convert potential to V.
        inner1 = -self.solution.salt1_Z * e * (potential / 1000)
        inner1 = inner1 / (KB * self.solution.T)
        concentration1 = self.solution.salt1_conc * np.exp(inner1)

        # Must convert potential to V.
        inner2 = -self.solution.salt2_Z * e * (potential / 1000)
        inner2 = inner2 / (KB * self.solution.T)
        concentration2 = self.solution.salt2_conc * np.exp(inner2)

        return concentration1, concentration2

    def interpolate_solution(self):
        """
        Interpolate numerical solution to get psi(r) and derivatives.

        """
        psi = self.pde_solution.sol(self.x_space)[0]
        # first derivative of spline
        dpsidr = self.pde_solution.sol.derivative(nu=1)(
            self.x_space
        )[0]
        # second derivative of spline
        d2psidr2 = self.pde_solution.sol.derivative(nu=2)(
            self.x_space
        )[0]

        return psi, dpsidr, d2psidr2

    def reduce_sigma(self):
        """
        Convert sigma to reduced units.

        Defined by:
        sigma' = (e * sigma * chain_radius) / e_0 E_r * k_B * T
        - sigma here must be in units of C/m^2
        - chain_radius must be in units of m

        """
        # Convert chain radius to m.
        a = self.a * (10 ** -10)
        # Convert sigma to C/m^2.
        sigma = self.sigma * e * (10 ** 20)

        # Units = C/m^2 * C * m = C^2/m
        top = sigma * a * e
        # Units = C^2 J^-1 m^-1 J
        bottom = E_0 * self.solution.E_r * self.solution.T * KB

        sigma_r = top / bottom
        self.sigma_r = sigma_r

    def calc_chain_distance(self):
        """
        Calculate the distance between chains.

        Used as unit cell radius R in model.
        Defined in Equation 1 of
        https://pubs.acs.org/doi/full/10.1021/ma0106604 as:

        R = (MW_per_disacc  / 2 pi b NA C_gag) ^ (1/2)

        """
        top = self.MW_per_disacc
        bottom = 2 * PI * NA * self.b_m * self.C_gag

        self.R = (top / bottom) ** (1/2)

    def boundary_conditions(self, ya, yb):
        """
        Define the boundary conditions of the model.

        Boundary conditions for Poisson-Boltzmann equation in
        cylindrical coordinates in reduced units:

        From: https://doi.org/10.1073/pnas.032480699

        BC 1 at x = a' [reduced]:
            d(psir)/dx |x=a' = -sigma' / a'

        BC 2 at x = R' [reduced]:
            d(psir)/dx |x=R' = 0


        Notes
        -----
        ya[0] implies the function y at first boundary a
        ya[1] implies the function y' at first boundary a
        yb[0] implies the function y at second boundary b
        yb[1] implies the function y' at second boundary b

        """

        value = -self.sigma_r*(self.a_r ** -1)

        res = [
            ya[1] - value,
            yb[1]
        ]
        return res

    def initial_guess(self, x):
        """
        Define the intial conditions for numerical solution.

        Defined as 0 for all x.

        Notes
        -----


        """

        yinit = np.zeros((2, x.size))
        return yinit

    def reduced_PB_expression(self, x, y):
        """
        Defines Poisson-Boltzmann expression in reduced units.

        Definition:

        1) From nonlinear Poisson-Boltzmann equation:

        grad^2 psi = (-e/E_0*E_r) [
            sum_i(pm) c_i*z_i exp(-zi*e*psi / k_B*T)
        ] - rho(r)
                |-> goes to 0

        2) With cylindrical symmetry:
            grad^2 psi = d^2psi/dr^2 + 1/r dpsi/dr

        3) Define reduced units:
            phi = e*psi / k_B*T
            x = kappa * r
            c_i' = z_i * c_i / I
                (I = ionic strength)
            sigma' = sigma * e * a / (E_r * E_0 * k_B * T)

        4) Convert to reduced units.
            d^2phi/dx^2 =
                -1/2  * [sum_i c_i' exp(-z_i*phi)] - 1/x * dphi/dx

        5) Write two coupled, first order DEs

            y1 = phi
            -> y1' = dphi/dx

            y2 = dphi/dx
            -> y2' = -1/2  * [sum_i c_i' exp(-z_i*y1)] - y2/x

        """

        inner = (
            self.z_1 * self.n_1 * np.exp(-self.z_1 * y[0]) +
            self.z_2 * self.n_2 * np.exp(-self.z_2 * y[0])
        )
        value = ((-1/2) * inner / self.I_m3) - y[1]/x

        Y = np.vstack([
            y[1],
            value
        ])

        return Y
