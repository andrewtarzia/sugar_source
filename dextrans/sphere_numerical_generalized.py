#!/usr/bin/env python

"""
Calculate electrostatic potential and zinc ion enhancement surounding
an ion-permeable sphere model of functionalised carbohydrates.

Author: Andrew Tarzia

"""

import numpy as np
from scipy import integrate
from scipy import constants
import expressions_sphere_script as exp_s
import out_fun as pf
import parameters

# plot set
plot_analytic = False
plot_surf = True
plot_phi_cf = False
plot_HS_diff = False
plot_cge_fn = True

# constants
KB = constants.Boltzmann  # boltzmann constant [J/K]
NA = constants.N_A  # avogadros number [mol-1]
e = constants.elementary_charge  # elementary charge [C]
E_0 = constants.epsilon_0  # Vacuum permittivity [F/m = C^2/(J.m)]
M_TO_NM = 1.0e9  # conversion from m to nm
M3_TO_L = 1.0e3  # conversion from m^3 to L
PI = constants.pi


def fun_red(xx, y):
    """Poisson-Boltzmann equation in spherical coordinates.
    Spherical - reduced coordinates
    reduced parametrs: psi(r) = PSI

    y(0)  = psi(r)
    y(0)' = dpsi / dr = y(2) / r^2
    y(1)  = r^2 (dpsi / dr) = r^2 * y(1)
    y(1)' = r^2 ( (-e / epsilon)[sum(zi ni exp(-zi*psi(r)))]
                - (rho(r) / epsilon)  )

    rr in m
    everything in SI
    n+, n- in: m^-3

    """
    global AX0
    global w
    global x0
    global z_1
    global z_2
    global n_1
    global n_2
    global I_m3

    # ax = np.zeros(xx.size)
    # ax[xx <= x0] = AX0

    # calculate smooth function AX_f to avoid numerical issues
    AX_f = -(AX0/2) * (np.tanh((xx - x0) / (2 * w)) - 1)
    inner = (z_1 * n_1 * np.exp(-z_1 * y[0]) + z_2 * n_2 * np.exp(-z_2 * y[0]))

    # y1 = phi, y2 = dphi/dx
    dydr = np.vstack([y[1],
                      -inner/2/I_m3 - AX_f/2 - 2 * y[1]/xx])

    return dydr


def bc(ya, yb):
    """BCs for Poisson-Boltzmann equation in cylindrical coords.
    Boundary conditions for Poisson-Boltzmann equation in cylindrical
    coordinates in reduced units:
      y(1) = psir = z*e/(k*T)*psi
      y(2) = rr*dpsir/drr  (rr = r/radius)
      y(2) = 0 at r=0
      y(2) = z*e*sig*radius/(eps0*eps*k*T) = sigr

    redefined:
        if:
          y = psir = z*e/(k*T)*psi
        BCs:
          y' = 0 at r=0
          y  = 0 at r=R

    notes:
        ya[0] implies the function y at first boundary a
        ya[1] implies the function y' at first boundary a
        yb[0] implies the function y at second boundary b
        yb[1] implies the function y' at second boundary b

    """

    res = [ya[1],
           yb[0]]
    return res


def pb_cylinder_init(rr, inner, outer, inner_d1, outer_d1,
                     inner_d2, outer_d2, A):
    """Initial guess for solution to PB eqn in spherical coords.

    initial mesh:
        [from analytical solution Ohshima, 93]
        at r == 0 (y[0, 0]),
                potential = rho/(eps_0 * eps_r * kappa ** 2)
             == e * rho / (kb * T * eps_0 * eps_r * kappa ** 2)
             in reduced potential units
        at r = Rmax (y[0, 1]), potential == 0
    """
    # Set the initial guess to 0 for all r
    # this is how it was done in the manuscript. The following three
    # cases showed no significant changes to the final results
    yinit = np.zeros((2, rr.size))

    # Set the initial solution to be equal to the analytical solution
    # y1 = phi, y2 = dphi/dx
    # yinit[0] = np.append(inner, outer)
    # yinit[1] = np.append(inner_d1, outer_d1)
    # y1 = phi, y2 = x^2 * dphi/dx
    # yinit[0] = np.append(inner, outer)
    # yinit[1] = np.append(inner_d1, outer_d1) * (rr ** 2)

    # Set the initial solution to be equal to 0 in all places except
    # at s smallest value of rr, where it is set to the value as r goes
    # to 0 of the analytical solution
    # y1 = phi, y2 = dphi/dx
    # yinit[0][0] = A/2
    # yinit[1][0] = 0

    return yinit


if __name__ == '__main__':
    result_file = 'results.csv'
    with open(result_file, 'w') as f:
        f.write('prefix,rg,kappa_ang,x0,func,ds,w,')
        f.write('vol,vq_si,ax0,Q,Qi\n')

    result_dict = pf.initialise_res_dict()
    print('get polymer propeties')
    Rg, Vg, DS, DP, Lc = parameters.polymer_prop()
    print('get solution propeties')
    sol_prop = parameters.solution_properties()
    print('get functionalisation properties')
    func_colors, func_charges, func_pkas, w = parameters.func_prop()

    # set constant global parameters
    n_1 = sol_prop['c1'] * NA * M3_TO_L  # m^-3
    n_2 = sol_prop['c2'] * NA * M3_TO_L  # m^-3
    z_1 = sol_prop['z1']
    z_2 = sol_prop['z2']
    I_m3 = sol_prop['I_m3']

    # run calculation
    kappa_ang = sol_prop['kappa_a']
    r_in = np.linspace(1E-1, 100, 100)
    r_out = np.linspace(0.1, 100, 1000)
    x_in = r_in * kappa_ang
    x_out = r_out * kappa_ang

    x0 = Rg * kappa_ang
    print('reduced Rg =', x0)

    # iterate over functionality
    for func in func_pkas.keys():
        print('-----------------------------------------------------')
        print("functionalisation:", func)
        print('-----------------------------------------------------')
        # dummy
        solution = 0
        plt_2_x = []
        plt_2_y = []
        plt_3_x = []
        plt_3_y = []
        # iterate over DS
        for i, ds in enumerate(DS):
            print('--------------------------------')
            print('ds =', ds)
            print('get polymer charge properties')
            func_total_charges = parameters.charge_properties(
                Vg, ds, func,
                func_charges,
                func_pkas,
                sol_prop['PH'],
                DP
            )

            # get volume charge density
            Vq = func_total_charges / Vg  # e / angstrom^3
            Vq_si = Vq * e * (10 ** 30)  # C/m^3
            AX0 = Vq_si / sol_prop['I_m3'] / e  # unit less
            print('volume charge density =', Vq, 'Rg =', Rg)
            print('A(x=0) =', AX0, 'X0 =', x0)
            # get smooth function
            print('Smooth function width (RED) =', w)
            print('Smooth function width (SI) =', w / kappa_ang)
            AX_f = exp_s.get_smooth_fn(AX0, x_in, x0, w)
            # and its associated total charge
            AX_f_v_int = exp_s.get_Q_from_smooth_fn(
                AX_f, e,
                sol_prop,
                x_in/kappa_ang,
                PI
            )
            perc = round(
                (func_total_charges - AX_f_v_int)/func_total_charges,
                4
            )
            print(
                f'total charge = {round(func_total_charges, 4)} e -- '
                f'num. vol. int. = {round(AX_f_v_int, 4)} e -- '
                f'diff = {round(func_total_charges - AX_f_v_int, 4)} e'
                f' -- rel. error = {perc * 100} %'
            )

            # use the analytical solution by Ohshima as initial guess
            x_in_1 = x_in[x_in <= x0]
            x_in_2 = x_in[x_in > x0]

            if i == 0:
                print('anayltical solution as initial guess')
                x_in_1 = x_in[x_in <= x0]
                x_in_2 = x_in[x_in > x0]
                inner_, outer_ = exp_s.a_sol_r(AX0, x0,
                                               x_in_1, x_in_2)
                inner_d1, outer_d1 = exp_s.a_deriv_r(AX0, x0,
                                                     x_in_1, x_in_2)
                solInit = pb_cylinder_init(x_in,
                                           inner_, outer_,
                                           inner_d1, outer_d1,
                                           None, None,
                                           AX0)
                y = solInit
            else:
                print('previous solution as initial guess')
                # remove some points from the initial solution as we
                # do not want all of the added mesh nodes near Rg
                x_in = solution.x[
                    (solution.x > x0+0.1) | (solution.x < x0-0.1)
                ]
                y = np.asarray([
                    solution.y[0][
                        (solution.x > x0+0.1) | (solution.x < x0-0.1)
                    ],
                    solution.y[1][
                        (solution.x > x0+0.1) | (solution.x < x0-0.1)
                    ]
                ])

            # solve!
            # fun = RHS of system of ODEs
            # bc = boundary conditions
            # in_x = initial mesh
            # y = initial guess
            solution = integrate.solve_bvp(fun_red, bc, x_in, y,
                                           max_nodes=1000,
                                           verbose=False)
            print(solution.success, solution.message, solution.status)

            # save to output
            # update result dict and save to results file
            pf.update_result_dict_file(
                result_file, result_dict, func,
                i, ds, Rg, kappa_ang, x0, w, Vg, Vq_si,
                AX0, func_total_charges, AX_f_v_int
            )

            # save continuous data to files -- given by filename
            prefix = 'cts_'+str(func)+'_'+str(i)
            pf.output_cts_data(
                prefix, solution, r_out, x_out, KB, sol_prop, e
            )
            result_dict = pf.initialise_res_dict()

    print('------------------------------------------------------')
    print('all analysis done.')
    print('------------------------------------------------------')
