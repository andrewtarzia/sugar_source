#!/usr/bin/env python

"""
Calculate zinc ion enhancement of ion-permeable sphere model of functionalised
carbohydrates.

"""

import numpy as np
# replace matlab BVP solver with scipy
from scipy.integrate import solve_bvp
from scipy import constants
from sys import exit
import expressions_sphere_script as exp_s
import out_fun as pf

__author__ = "Andrew Tarzia"
__copyright__ = ""
__credits__ = ["Andrew Tarzia"]
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"
__status__ = "Production"

# constants
KB = constants.Boltzmann  # boltzmann constant [J/K]
NA = constants.N_A  # avogadros number [mol-1]
e = constants.elementary_charge  # elementary charge [C]
E_0 = constants.epsilon_0  # Vacuum permittivity [F/m = C^2/(J.m)]
M_TO_NM = 1.0e9  # conversion from m to nm
M3_TO_L = 1.0e3  # conversion from m^3 to L
PI = constants.pi

# polymer properties
# Lc is approximate monomer length (length of Glucose unit)
# Lc = 5  # angstrom
# using value from "Polysaccharide elasticity governed by chair±boat
#                   transitions of the glucopyranose ring"
Lc = 4.4  # angstrom
# DP is the number of monomers in carbohydrate chain
# or degree of polymerisation DP
# set such that MW is around 10 kDa
DP = 60

# from literature:
# - Carboxymethyl and native dextran behave as freely jointed chains
# - Kuhn length is approximately the same as the length of the glucose monomer
# calculate radius of gyration Rg using equation for Freely Jointed Chain
Rg_2 = (DP * (Lc ** 2)) / 6
Rg = np.sqrt(Rg_2)
print("-------------------------------")
print("radius of gyration =", round(Rg, 3), "angstrom")
# calculate the pervaded volume as the volume of a sphere with radius == Rg
Vg = (4/3) * PI * (Rg ** 3)
print("pervaded volume =", round(Vg, 3), "angstrom^3")
print("-------------------------------")

# solution properties
# set temp
T = 298  # [K]
# set relative permittivity [unit less] -- water
E_r = 78.46
# set pH of solution
PH = 11
# salt concs (1 = Zn2+, 2 = acetate1-)
salt1_conc = 0.04  # mol/L
salt2_conc = 0.08  # mol/L
# bulk salt conc (total)
salt_conc = salt1_conc + salt2_conc  # mol/L
# ion valencies
Z1 = 2  # zinc 2+
Z2 = -1  # co-ion

# functionalisation properties
# set plot colours for the different functionalities
func_colors = {"COO": 'firebrick', "NH2": 'royalblue'}

# set functionalisation density as degree of substitution:
# DS = avg number of substituted OH for R per glucose
# (max at 3, for 3 OH per glucose)
# vary the degree of substitution
# DS = np.linspace(0, 1, 3)
DS = np.append(np.linspace(0, 0.05, 5), np.linspace(0.1, 1.0, 5))
# the total number of charge centres on the chain based on the DS and DP
func_count = DS * DP

# set charge and pka of functionalisations
func_charges = {"NH2": +1, "COO": -1}
func_pkas = {"COO": 4, "NH2": 10.64}
# NH2 pKa: DOI:10.1021/je500680q for triethylamine at 298 K
# COO pka:
print("-------------------------------")
print("I need to update the pKas.")
print("-------------------------------")

# calculate cylinder charge of each functionalisation based on pH
func_total_charges = {}
for func in func_charges.keys():
    pka = func_pkas[func]
    charge = func_charges[func]
    partial_charge = exp_s.calculate_func_charge(charge, PH, pka)
    func_total_charges[func] = func_count * partial_charge

# plot
pf.plot_charge_properties('spherical_tc.pdf',
                          'spherical_vcd.pdf',
                          func_charges=func_charges, func_colors=func_colors,
                          func_total_charges=func_total_charges, Vg=Vg, DS=DS)


# BVP functions
def fun_red(xx, y):
    """Poisson-Boltzmann equation in spherical coordinates.
    Spherical - reduced coordinates
    reduced parametrs: psi(r) = PSI

    y(0)  = psi(r)
    y(0)' = dpsi / dr = y(2) / r^2
    y(1)  = r^2 (dpsi / dr) = r^2 * y(1)
    y(1)' = r^2 ( (-e / epsilon)[sum(zi ni exp(-zi*psi(r)))]
                - (rho(r) / epsilon)  )

    rr in m
    everything in SI
    n+, n- in: m^-3

    """
    global z_m
    global z_p
    global n_p
    global n_m
    global epsilon
    global AX
    global rg
    global KB
    global T
    global e
    global I_m3

    # print(z_m, z_p, n_p, n_m, epsilon, vg, rg, KB, T, e)

    ax = np.zeros(xx.size)
    # print('xx', xx)
    # print(rg)
    ax[xx <= rg] = AX
    # print(ax)
    # if AX > 0:
    #     import sys
    #     # sys.exit()
    inner = (z_p * n_p * np.exp(-z_p * y[0]) + z_m * n_m * np.exp(-z_m * y[0]))

    # y1 = phi, y2 = dphi/dx
    # dydr = np.vstack([y[1],
    #                   ((-1 / (2 * I_m3)) * inner)
    #                     - (ax / 2) - (2 * y[1] / xx)])

    # y1 = phi, y2 = x^2 * dphi/dx
    dydr = np.vstack([y[1] / (xx ** 2),
                      (xx ** 2) * (((-1 / (2 * I_m3)) * inner) - (ax / 2))])

    return dydr


def fun_red_lin(xx, y):
    """Poisson-Boltzmann equation in spherical coordinates.
    Spherical - reduced coordinates
    reduced parametrs: psi(r) = PSI

    y(0)  = psi(r)
    y(0)' = dpsi / dr = y(2) / r^2
    y(1)  = r^2 (dpsi / dr) = r^2 * y(1)
    y(1)' = r^2 ( (-e / epsilon)[sum(zi ni exp(-zi*psi(r)))]
                - (rho(r) / epsilon)  )

    rr in m
    everything in SI
    n+, n- in: m^-3

    """
    global z_m
    global z_p
    global n_p
    global n_m
    global epsilon
    global AX
    global rg
    global KB
    global T
    global e
    global I_m3

    # print(z_m, z_p, n_p, n_m, epsilon, vg, rg, KB, T, e)

    ax = np.zeros(xx.size)
    ax[xx <= rg] = AX
    # y1 = phi, y2 = dphi/dx
    dydr = np.vstack([y[1],
                      y[0] - (ax / 2) - (2 * y[1] / xx)])

    return dydr


def bc(ya, yb):
    """BCs for Poisson-Boltzmann equation in cylindrical coords.
    Boundary conditions for Poisson-Boltzmann equation in cylindrical
    coordinates in reduced units:
      y(1) = psir = z*e/(k*T)*psi
      y(2) = rr*dpsir/drr  (rr = r/radius)
      y(2) = 0 at r=0
      y(2) = z*e*sig*radius/(eps0*eps*k*T) = sigr

    redefined:
        if:
          y = psir = z*e/(k*T)*psi
        BCs:
          y' = 0 at r=0
          y  = 0 at r=R

    notes:
        ya[0] implies the function y at first boundary a
        ya[1] implies the function y' at first boundary a
        yb[0] implies the function y at second boundary b
        yb[1] implies the function y' at second boundary b

    """

    res = [ya[1],
           yb[0]]
    return res


def pb_cylinder_init(rr, inner, outer, inner_d1, outer_d1, inner_d2, outer_d2):
    """Initial guess for solution to PB eqn in spherical coords.

    initial mesh:
        [from analytical solution Ohshima, 93]
        at r == 0 (y[0, 0]), potential = rho/(eps_0 * eps_r * kappa ** 2)
             == e * rho / (kb * T * eps_0 * eps_r * kappa ** 2)
             in reduced potential units
        at r = Rmax (y[0, 1]), potential == 0
    """

    yinit = np.zeros((2, rr.size))
    # y1 = phi, y2 = dphi/dx
    # yinit[0] = np.append(inn`er, outer)
    # yinit[1] = np.append(inn`er_d1, outer_d1)
    # y1 = phi, y2 = x^2 * dphi/dx
    yinit[0] = np.append(inner, outer)
    yinit[1] = np.append(inner_d1, outer_d1) * (rr ** 2)
    return yinit


if __name__ == '__main__':
    print("-------------------------------")
    # calculate ionic strength
    I_mol = 0.5 * ((salt1_conc * Z1 * Z1) + (salt2_conc * Z2 * Z2))  # mol/L
    print('I =', I_mol, 'mol/L')
    # in molec per m3
    I_m3 = NA * I_mol * M3_TO_L  # molecules / m3
    print('I =', I_m3, 'm^-3')
    print("-------------------------------")
    # calculate kappasq
    kappasq = exp_s.calculate_kappasq(e, I_m3, E_0, E_r, KB, T, PI)  # m^-2
    kappa = np.sqrt(kappasq)  # m^-1
    print('kappa^2 =', round(kappasq, 3), 'm^-2')
    print('kappa =', kappa * (10 ** -10), 'angstrom^-1')
    print('debye length =',
          round(1/(kappa * (10 ** -10)), 3), 'angstrom')
    print("-------------------------------")

    resolution = 0.1  # angstrom
    max_r = 100  # angstrom
    radius_to_incl = 0.74  # zinc ion radius in angstrom
    # number of nodes to include radius on either side
    nodes_incl = int(radius_to_incl / resolution)

    # radial positions from centre of cylinder in angstrom
    # initial guess at solution.
    # we know that at r == 0 (centre of sphere) the potential
    # must <= XX from Wall paper (https://doi.org/10.1063/1.1743234)
    # we know that at r == Rmax (or infinity), the potential must  == 0
    # r_in = np.asarray([0.000000001, max_r])

    # use the analytical solution by Ohshima as initial guess
    r_in = np.linspace(1E-4, max_r, 100)
    r_out = np.linspace(0.01, max_r, int(max_r / resolution))

    Rg_si = Rg * (10 ** -10)  # m
    r_in_si = r_in * (10 ** -10)  # m
    r_out_si = r_out * (10 ** -10)  # m
    n_p_si = salt1_conc * NA * M3_TO_L  # m^-3
    n_m_si = salt2_conc * NA * M3_TO_L  # m^-3

    fig, ax1, ax2, ax3 = pf.initialise_final_plot()

    func_surface_concentration = {}
    solution = 0
    for func in func_charges.keys():
        print("-------------------------------")
        print(func)
        # collect data for each DS
        psi_ = []
        for i, ds in enumerate(DS):
            # calculate volume charge density as total charge / Volume
            Vg_q = func_total_charges[func][i] / Vg  # e/angstrom^3
            Vg_q_si = Vg_q * e * (10 ** 30)  # C/m^3

            # set params for BVP
            vg = Vg_q_si
            rg = Rg_si * kappa
            n_p = n_p_si
            n_m = n_m_si
            z_p = Z1
            z_m = Z2
            epsilon = E_0 * E_r

            # print('pre')
            # print(z_m, z_p, n_p, n_m, epsilon, vg, rg, KB, T, e)

            # get intial solution
            print(i)
            if i <= 1:
                print('analytical solution is initial quess')
                # calculate analytical solution from Ohshima 93
                kappa_si = kappa
                kappa_a_si = kappa_si * Rg_si  # unit less
                r_in_1_si = r_in_si[r_in_si <= Rg_si]
                r_in_2_si = r_in_si[r_in_si > Rg_si]

                AX = vg / (I_m3 * e)  # unit less

                x_inner = r_in_1_si * kappa_si
                x_outer = r_in_2_si * kappa_si

                inner_, outer_ = exp_s.a_sol_r(AX, kappa_a_si,
                                               x_inner, x_outer)
                inner_d1, outer_d1 = exp_s.a_deriv_r(AX, kappa_a_si,
                                                     x_inner, x_outer)
                inner_d2, outer_d2 = exp_s.a_deriv2_r(AX, kappa_a_si,
                                                      x_inner, x_outer)
                # print('plot analytical solution')
                # ax1.plot(r_in, np.append(inner_, outer_) * 1000 * KB * T / e,
                #          c=func_colors[func], linestyle='--')
                # ax1.plot(r_in, np.append(inner_d1, outer_d1), c='b')
                # ax1.plot(r_in, np.append(inner_d2, outer_d2), c='r')
                # continue

                in_x = r_in_si * kappa_si  # unit less
                solInit = pb_cylinder_init(in_x,
                                           inner_, outer_,
                                           inner_d1, outer_d1,
                                           inner_d2, outer_d2)
                y = solInit
            else:
                print('previous solution is initial quess')
                # use previous solution as input to current run
                in_x = solution.x
                y = solution.y

            # ax = np.zeros(in_x.size)
            # print(rg)
            # ax[r_in <= Rg] = AX
            # ax1.plot(r_in, ax, c='r')
            # continue
            # print(in_x)
            # print(y)
            # reduce all variables
            # y is already reduceed
            AX = vg / (I_m3 * e)  # unit less
            print('volume charge density =', AX, 'Rg =', rg)
            # all other reductions occur in fun_red

            # solve!
            # fun = RHS of system of ODEs
            # bc = boundary conditions
            # in_x = initial mesh
            # y = initial guess
            solution = solve_bvp(fun_red, bc, in_x, y,
                                 max_nodes=1000,
                                 verbose=False, tol=1E-0)
            # print(ds, solution.success, solution.message, solution.status)

            # print('plot residuals:')
            # print(solution.rms_residuals)
            # ax1.plot(solution.x[:-1], solution.rms_residuals,
            #          c=func_colors[func])
            # ax1.axvline(kappa_a_si,
            #             c='k', alpha=0.4)

            # print(solution.y[0])
            # reduced radial coord. (for output)
            # reduced electrostatic potential
            # output the values of sol for all r
            psir = solution.sol(r_out_si * kappa_si)[0]  # reduced

            # electrostatic potential [mV]
            psi = psir * 1000 * KB * T / e
            ax1.plot(r_out, psi, c=func_colors[func])

            # get avg potential around point a
            region_ = psi[(r_out > Rg-radius_to_incl) & (r_out < Rg+radius_to_incl)]
            # print(region_)
            ax2.scatter(ds, np.average(region_), c=func_colors[func],
                        s=20)
            # calculate and plot analytical solution
            if ds < 0.2:
                r_out_1_si = r_out_si[r_out_si <= Rg_si]
                r_out_2_si = r_out_si[r_out_si > Rg_si]
                x_inner = r_out_1_si * kappa_si
                x_outer = r_out_2_si * kappa_si
                inner_, outer_ = exp_s.a_sol_r(AX, kappa_a_si,
                                               x_inner, x_outer)
                ax2.scatter(ds,
                            np.average([inner_[-1], outer_[0]])* 1000 * KB * T / e,
                            c=func_colors[func],
                            marker='x')

            # get zinc ion enhancement at surface from potential
            zinc_conc = exp_s.calculate_conc(np.average(region_)/1000,
                                             salt1_conc, Z1, e, KB, T)
            print(zinc_conc)
            print(zinc_conc/salt1_conc)
            ax3.scatter(ds, zinc_conc/salt1_conc, c=func_colors[func])

    # legend and plot one plot limits
    ax1.set_xlim(0, max_r)
    # ax1.set_ylim(0, 2)
    ax2.set_ylim(-100, 100)
    # legend
    # for func in func_charges.keys():
    #     ax1.plot(-10000, -10000, c=func_colors[func], label=func)

    ax1.axvspan(xmin=Rg-radius_to_incl, xmax=Rg+radius_to_incl,
                facecolor='k', alpha=0.4)

    # ax1.legend(fancybox=True, ncol=1, loc=4, fontsize=16)
    pf.finalise_final_plot(fig, ax1, ax2, ax3,
                           'spherical_sum.pdf')
